﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A009.Exercise
{
    internal class Helicopter
    {
        public string ID { get; set; }
        public string Model { get; set; }
        public float CruiseSpeed { get; set; }
        public float EmptyWeight { get; set; }
        public float MaxTakeoffWeight { get; set; }
        public float Range { get; set; }

        public Helicopter()
        {
            this.ID = "";
            this.Model = "";
        }

        public Helicopter(string iD, string model, float cruiseSpeed, float emptyWeight, float maxTakeoffWeight, float range)
        {
            ID = iD;
            Model = model;
            CruiseSpeed = cruiseSpeed;
            EmptyWeight = emptyWeight;
            MaxTakeoffWeight = maxTakeoffWeight;
            Range = range;
        }

        public void Fly()
        {
            Console.WriteLine("Rotated wing");
        }
        public Helicopter InputHelicopter(bool id = true, bool model = true, bool cruiseSpeed = true,
                                    bool empWeight = true, bool maxTakeoffWeight = true, bool range = true)
        {
            Helicopter res = new Helicopter();
            Console.WriteLine("InputSomethingOfType Helicopter information:");
            if (id) res.ID = NDF.InputID("RW");
            if (model) res.Model = NDF.InputFixedWingModel();
            if (cruiseSpeed) res.CruiseSpeed = NDF.InputSomethingOfType<float>("Cruise speed: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            if (empWeight) res.EmptyWeight = NDF.InputSomethingOfType<float>("Empty weight: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            if (maxTakeoffWeight) res.MaxTakeoffWeight = NDF.InputNumberNotExceed(res.EmptyWeight, 1.5f, "Input airplane max take off weight");
            if (range) res.Range = NDF.InputSomethingOfType<float>("Range: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            return res;
        }
    }
}
