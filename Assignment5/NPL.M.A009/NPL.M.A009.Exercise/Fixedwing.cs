﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A009.Exercise
{
    public enum FixedwingType
    {
        CAG,
        LGR,
        PRV
    }

    internal class Fixedwing
    {
        public string ID { get; set; }
        public string Model { get; set; }
        public FixedwingType PlaneType { get; set; }
        public float CruiseSpeed { get; set; }
        public float EmptyWeight { get; set; }
        public float MaxTakeoffWeight { get; set; }
        public float MinNeededRunwaySize { get; set; }

        public Fixedwing()
        {
            this.ID = string.Empty;
            this.Model = string.Empty;
        }

        public Fixedwing(string iD, string model, FixedwingType planeType, float cruiseSpeed, 
                        float emptyWeight, float maxTakeoffWeight, float minNeededRunwaySize)
        {
            ID = iD;
            Model = model;
            PlaneType = planeType;
            CruiseSpeed = cruiseSpeed;
            EmptyWeight = emptyWeight;
            MaxTakeoffWeight = maxTakeoffWeight;
            MinNeededRunwaySize = minNeededRunwaySize;
        }

        public void Fly()
        {
            Console.WriteLine("Fixed wing");
        }

        public Fixedwing InputFixedwingAirplane(bool id = true, bool model = true, bool planeType = true, bool cruiseSpeed = true,
                                    bool empWeight = true, bool maxTakeoffWeight = true, bool minNeededRunwaySize = true)
        {
            Fixedwing res = new Fixedwing();
            Console.WriteLine("InputSomethingOfType FixedwingAirplane information:");
            if (id) res.ID = NDF.InputID("FW");
            if (model) res.Model = NDF.InputFixedWingModel();
            if (planeType) res.PlaneType = (FixedwingType)NDF.InputSomethingOfType<int>("1-CAR(Cargo)\n2-LGR(Long range)\n3-PRV(Private)\nChoose Plane type by an integer: ",
                "Only accept integer between 1 & 3", @"^[1-3]$");
            if (cruiseSpeed) res.CruiseSpeed = NDF.InputSomethingOfType<float>("Cruise speed: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            if (empWeight) res.EmptyWeight = NDF.InputSomethingOfType<float>("Empty weight: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            if (maxTakeoffWeight) res.MaxTakeoffWeight = NDF.InputSomethingOfType<float>("Max take off weight: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            if (minNeededRunwaySize) res.MinNeededRunwaySize = NDF.InputSomethingOfType<float>("Min needed runway size: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            return res;
        }
    }
}
