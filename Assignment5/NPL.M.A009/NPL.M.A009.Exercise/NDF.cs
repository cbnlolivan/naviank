﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace NPL.M.A009.Exercise
{
    internal static class NDF
    {
        public static string InputID(string prefix)
        {
            string message = "Input ID: ";
            string error = "ID must start with " + prefix.ToUpper() + " and be followed by 5 digits!";
            string regex = @"^(" + prefix.ToUpper() + "|" + prefix.ToLower() + @")\d{5}$";
            string res = InputStringOfRegexWithMessage(message, error, regex);
            return res.ToUpper();
        }
        public static string InputIDGeneric<T>()
        {
            string res = "";
            if (typeof(T) == typeof(Airport))
            {
                res = InputID("AP");
            }
            else if (typeof(T) == typeof(Fixedwing))
            {
                res = InputID("FW");
            }
            else if (typeof(T) == typeof(Helicopter))
            {
                res = InputID("RW");
            }
            return res;
        }
        public static string InputFixedWingModel()
        {
            string message = "Input model of of fixed-wing airplane: ";
            string error = "Model must not exceed 40 chars!";
            string regex = @"^.{1,40}$";
            string res = InputStringOfRegexWithMessage(message, error, regex);
            return res;
        }
        public static float InputNumberNotExceed(float multiplicand, float multiplier, string mess)
        {
            float result = 0;
            while (true)
            {
                mess += "This field must not exceed " + multiplicand * multiplier;
                Console.WriteLine(mess);
                Console.WriteLine("Input: ");
                //string tmp = Console.ReadLine();
                float tmp = InputSomethingOfType<float>(mess, "Wrong format, can not parse input string to real number!", @"^\d+\.?\d+$");
                if (tmp <= multiplicand * multiplier)
                {
                    Console.WriteLine("Accepted input!\n");
                    return result;
                }
                else
                {
                    Console.WriteLine("Input value has exceeded the limit value!!!");
                }
            }
        }
        // 
        public static T InputSomethingOfType<T>(string message, string errorMessage, string regex)
        {
            T result = default(T);
            while (true)
            {
                Console.Write(message);
                string tmp = Console.ReadLine();
                if (Regex.IsMatch(tmp, regex))
                {
                    try
                    {
                        Console.WriteLine("Accepted input:" + tmp);
                        Console.WriteLine();
                        result = (T)Convert.ChangeType(tmp, typeof(T));
                        return result;
                    }
                    catch (Exception ex) { }
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }
        }
        public static string InputStringOfRegexWithMessage(string message, string error, string regex)
        {
            while (true)
            {
                Console.Write(message);
                string res = Console.ReadLine();
                if (Regex.IsMatch(res, regex))
                {
                    Console.WriteLine("Accepted input:" + res);
                    Console.WriteLine();
                    return res;
                }
                else
                {
                    Console.WriteLine(error);
                }
            }
        }
        public static bool PressYNToContinue(string mess)
        {
            bool res = "Y".Equals(InputStringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
            return res;
        }

        // Inputter
        public static Fixedwing InputFixedwingAirplane()
        {
            Fixedwing res = new Fixedwing();
            Console.WriteLine("Input FixedwingAirplane information:");
            res.ID = InputID("FW");
            res.Model = InputFixedWingModel();
            res.PlaneType = (FixedwingType)InputSomethingOfType<int>("1-CAR(Cargo)\n2-LGR(Long range)\n3-PRV(Private)\nChoose Plane type by an integer: ",
                "Only accept integer between 1 & 3", @"^[1-3]$");
            res.CruiseSpeed = InputSomethingOfType<float>("Cruise speed: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            res.EmptyWeight = InputSomethingOfType<float>("Empty weight: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            res.MaxTakeoffWeight = InputSomethingOfType<float>("Max take off weight: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            res.MinNeededRunwaySize = InputSomethingOfType<float>("Min needed runway size: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            return res;
        }
        public static Helicopter InputHelicopter()
        {
            Helicopter res = new Helicopter();
            Console.WriteLine("Input Helicopter information:");
            res.ID = InputID("RW");
            res.Model = InputFixedWingModel();
            res.CruiseSpeed = InputSomethingOfType<float>("Cruise speed: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            res.EmptyWeight = InputSomethingOfType<float>("Empty weight: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            res.MaxTakeoffWeight = InputNumberNotExceed(res.EmptyWeight, 1.5f, "Input airplane max take off weight");
            res.Range = InputSomethingOfType<float>("Range: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            return res;
        }
        public static Airport InputNewAirport()
        {
            Airport res = new Airport();
            Console.WriteLine("Input Airport information:");
            res.ID = InputID("AP");
            res.Name = InputStringOfRegexWithMessage("Airport name: ", "Wrong name format!", @"^[a-zA-Z0-9 ]+$");
            res.RunwaySize = InputSomethingOfType<float>("Airport runway size: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            res.MaxFixedwingParkingPlace = InputSomethingOfType<int>("Max fixed-wing parking place: ", "Accept integer only!", @"^[0-9]+$");
            res.MaxRotatedWingParkingPlace = InputSomethingOfType<int>("Max rotated-wing parking place: ", "Accept integer only!", @"^[0-9]+$");
            res.AddFixedWingAirplaneToList();
            res.AddHelicopterToList();
            return res;
        }
        //
    }
}
