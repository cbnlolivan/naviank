﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A009.Exercise
{
    internal class Airport
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public float RunwaySize { get; set; }
        public int MaxFixedwingParkingPlace { get; set; }
        public List<string> ListofFixedWingAirplaneID { get; set; }
        public int MaxRotatedWingParkingPlace { get; set; }
        public List<string> ListofHelicopterID { get; set; }

        // Constructor
        public Airport()
        {
            ID = "";
            Name = "";
            ListofHelicopterID = new List<string>();
            ListofFixedWingAirplaneID = new List<string>();
        }
        public Airport(string iD, string name, float runwaySize, int maxFixedwingParkingPlace, int maxRotatedParkingPlace)
        {
            ID = iD;
            Name = name;
            RunwaySize = runwaySize;
            MaxFixedwingParkingPlace = maxFixedwingParkingPlace;
            MaxRotatedWingParkingPlace = maxRotatedParkingPlace;
            ListofHelicopterID = new List<string>();
            ListofFixedWingAirplaneID = new List<string>();
        }
        public Airport(string iD, string name, float runwaySize, int maxFixedwingParkingPlace, List<string> listofFixedWingAirplaneID,
                                                               int maxRotatedParkingPlace, List<string> listofHelicoptergID)
        {
            ID = iD;
            Name = name;
            RunwaySize = runwaySize;
            MaxFixedwingParkingPlace = maxFixedwingParkingPlace;
            ListofFixedWingAirplaneID = listofFixedWingAirplaneID;
            MaxRotatedWingParkingPlace = maxRotatedParkingPlace;
            ListofHelicopterID = listofHelicoptergID;
        }
        // Add methods
        public bool AddOneFixedWingAirplane(Fixedwing fixedWing)
        {
            if (ListofFixedWingAirplaneID.Contains(fixedWing.ID))
            {
                Console.WriteLine("Airplane had existed int the airport!");
                return false;
            }
            if (fixedWing.MinNeededRunwaySize >= this.RunwaySize)
            {
                Console.WriteLine("Min needed runway size exceed Airport runway size! Added fail!");
                return false;
            }
            this.ListofFixedWingAirplaneID.Add(fixedWing.ID);
            return true;
        }
        public bool AddOneHelicopter(Helicopter helicopter)
        {
            if (ListofHelicopterID.Contains(helicopter.ID))
            {
                Console.WriteLine("Helicopter had existed int the airport!");
                return false;
            }
            this.ListofHelicopterID.Add(helicopter.ID);
            return true;
        }
        public void AddPlaneToList<T>()
        {
            dynamic tmp;
            while (NDF.PressYNToContinue("Press Y to continue adding: "))
            {
                if (typeof(T) == typeof(Helicopter))
                {
                    if (ListofHelicopterID.Count >= MaxRotatedWingParkingPlace)
                    {
                        Console.WriteLine("Airport is full of Helicopter!!!");
                        return;
                    }
                    tmp = new Helicopter();
                    tmp.InputHelicopter();
                    this.AddOneHelicopter(tmp);
                }
                else
                {
                    if (ListofFixedWingAirplaneID.Count >= MaxFixedwingParkingPlace)
                    {
                        Console.WriteLine("Airport is full of Airplane!");
                        return;
                    }
                    tmp = new Fixedwing();
                    tmp.InputFixedwingAirplane();
                    this.AddOneFixedWingAirplane(tmp);
                }
            }
        }

        // Can be removed methods
        public void AddFixedWingAirplaneToList()
        {
            while (NDF.PressYNToContinue("Press Y to add a fixed-wing airplane: "))
            {
                if (ListofHelicopterID.Count >= MaxRotatedWingParkingPlace)
                {
                    Console.WriteLine("Airport is full of Helicopter!!!");
                    return;
                }
                Fixedwing tmp = new Fixedwing();
                tmp.InputFixedwingAirplane();
                AddOneFixedWingAirplane(tmp);
            }
        }
        public void AddHelicopterToList()
        {
            while (NDF.PressYNToContinue("Press Y to add a helicopter: "))
            {
                if (ListofFixedWingAirplaneID.Count >= MaxFixedwingParkingPlace)
                {
                    Console.WriteLine("Airport is full of Airplane!!!");
                    return;
                }
                Helicopter tmp = new Helicopter();
                tmp.InputHelicopter();
                AddOneHelicopter(tmp);
            }
        }

        // Remove methods
        public bool RemoveAPlaneByID<T>()
        {
            string id;
            if (typeof(T) == typeof(Helicopter))
            {
                if (ListofHelicopterID.Count == 0)
                {
                    Console.WriteLine("There are no airplanes at this airport!");
                    return false;
                }
                id = NDF.InputIDGeneric<T>();
                if (ListofHelicopterID.Contains(id))
                {
                    ListofHelicopterID.Remove(id);
                    return true;
                }
            }
            else if (typeof(T) == typeof(Fixedwing))
            {
                if (ListofFixedWingAirplaneID.Count == 0)
                {
                    Console.WriteLine("There are no airplanes at this airport!");
                    return false;
                }
                id = NDF.InputIDGeneric<T>();
                if (ListofFixedWingAirplaneID.Contains(id))
                {
                    ListofFixedWingAirplaneID.Remove(id);
                    return true;
                }
            }
            return false;
        }
        public void RemoveMultiplePlane<T>()
        {
            int res = 0;
            do
            {
                if (RemoveAPlaneByID<T>() == true) 
                    res++;
                else 
                    break;
            } while (NDF.PressYNToContinue("Press Y to continue deleting plane: "));
        }

        // Input method 
        public void InputAirportAttribute(bool id = true, bool name = true, bool runwaySize = true,
                                 bool maxFW = true, bool maxRW = true)
        {
            Console.WriteLine("InputSomethingOfType Airport information:");
            if (id)
                this.ID = NDF.InputID("AP");
            if (name)
                this.Name = NDF.InputStringOfRegexWithMessage("Airport name: ", "Wrong name format!", @"^[a-zA-Z0-9 ]+$");
            if (runwaySize)
                this.RunwaySize = NDF.InputSomethingOfType<float>("Airport runway size: ", "Can not parse to real number!", @"^\d+\.?\d+$");
            if (maxFW)
            {
                this.MaxFixedwingParkingPlace = NDF.InputSomethingOfType<int>("Max fixed-wing parking place: ", "Accept integer only!", @"^[0-9]+$");
                this.AddPlaneToList<Fixedwing>();
            }
            if (maxRW)
            {
                this.MaxRotatedWingParkingPlace = NDF.InputSomethingOfType<int>("Max rotated-wing parking place: ", "Accept integer only!", @"^[0-9]+$");
                this.AddPlaneToList<Helicopter>();
            }
            //this.AddFixedWingAirplaneToList();
            //this.AddHelicopterToList();
        }



        public override string? ToString()
        {
            return "ID: " + ID + ", Name: " + Name + ", Runway size: " + RunwaySize +
                ", Airplane: " + ListofFixedWingAirplaneID.Count +
                ", Helicopter: " + ListofHelicopterID.Count;
        }
    }
}
