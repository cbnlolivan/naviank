﻿using NPL.M.A009.Exercise;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("==== NPL.M.A009 : Airport Management System ====\n");
        string menu = "Please select the function to continue: ";
        menu += "1. Input Data from keyboard";
        menu += "2. Airport management";
        menu += "\t2.1. Display list of all airport information (sort by airport ID)";
        menu += "\t2.2. Display status of one airport (sort by airport ID)";
        menu += "3. Fixed wing airplane management";
        menu += "\t3.1. Display list of all fixed wing airplanes and theirs position";
        menu += "\t3.2. Find a fixed wing airplane";
        menu += "4. Helicopter management";
        menu += "\t4.1. Display list of all helicopters and theirs position";
        menu += "\t4.2. Find a helicopter";
        menu += "0. Close program";
        Airport airport = new Airport();
        List<Fixedwing> listFW = InitFixedwing();
        List<Helicopter> listRW = InitHelicopters();
        List<Airport> listAP = InitAirport();
        Console.WriteLine(airport.ToString());
    }

    public static List<Airport> InitAirport()
    {
        List<Airport> list = new List<Airport>();
        list.Add(new Airport("AP00001", "Airport 1", 2000, 12, 12));
        list.Add(new Airport("AP00002", "Airport 2", 2400, 15, 15));
        list.Add(new Airport("AP00003", "Airport 3", 1200, 8, 8));
        list.Add(new Airport("AP00004", "Airport 4", 2800, 18, 18));
        list.Add(new Airport("AP00005", "Airport 5", 3200, 20, 20));
        return list;
    }

    public static List<Fixedwing> InitFixedwing()
    {
        List<Fixedwing> list = new List<Fixedwing>();
        list.Add(new Fixedwing("FW00001", "Airplane model 1", (FixedwingType) 3, 700, 10000, 15000, 1200));
        list.Add(new Fixedwing("FW00002", "Airplane model 2", (FixedwingType) 1, 1200, 12000, 18000, 1400));
        list.Add(new Fixedwing("FW00003", "Airplane model 3", (FixedwingType) 2, 800, 15000, 20000, 1600));
        list.Add(new Fixedwing("FW00004", "Airplane model 4", (FixedwingType) 3, 1000, 8000, 12000, 1000));
        return list;
    }

    public static List<Helicopter> InitHelicopters()
    {
        List<Helicopter> list = new List<Helicopter>();
        list.Add(new Helicopter("RW00001", "Helicopter model 1", 800, 700, 1200, 400));
        list.Add(new Helicopter("RW00002", "Helicopter model 2", 750, 600, 1200, 400));
        list.Add(new Helicopter("RW00003", "Helicopter model 3", 700, 500, 1200, 400));
        list.Add(new Helicopter("RW00004", "Helicopter model 4", 600, 400, 1200, 400));
        return list;
    }
}