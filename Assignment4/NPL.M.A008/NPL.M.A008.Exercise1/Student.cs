﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A008.Exercise
{
    internal class Student
    {
        public String Name { get; set; }
        public String Class { get; set; }
        public String Gender { get; set; }
        public String Relationship { get; set; }
        public DateOnly EntryDate { get; set; }
        public int Age { get; set; }
        public String Address { get; set; }
        public Decimal Mark { get; set; }
        public String Grade { get; set; }
        //public Student() { }

        public Student(string relationship = "Single", decimal mark = 0, string grade = "F")
        {
            Relationship = relationship;
            Mark = mark;
            Grade = grade;
        }

        public Student(string name, string @class, string gender, DateOnly entryDate, int age, string address, 
            String relationship = "Single", decimal mark = 0, string grade = "F")
        {
            this.Name = name;
            this.Class = @class;
            this.Gender = gender;
            this.Relationship = relationship;
            this.EntryDate = entryDate;
            this.Age = age;
            this.Address = address;
            this.Mark = mark;
            this.Grade = Graduate(mark);
        }

        public String ToString(String name = "Name", String @class = "Class", String gender = "Gender",
            String relationship = "Relationship", String age = "Age", String grade = "Grade")
        {
            //String res = String.Format("{0,-12} {1,-12} {2,-12} {3,-16} {4,-8} {5,-8}", 
            //    this.Name, this.Class, this.Gender, this.Relationship, this.Age, this.Grade); 
            String res = String.Format("{0,-12} {1,-12} {2,-12} {3,-16} {4,-8} {5,-8}", 
                name, @class, gender, relationship, age, grade);
            return res;
        }

        public string Graduate(decimal gradePoint = 0)
        {
            if (gradePoint < 49) { this.Grade = "F"; return "F"; }
            else if (gradePoint < 54) { this.Grade = "D"; return "D"; }
            else if (gradePoint < 59) { this.Grade = "C"; return "C"; }
            else if (gradePoint < 64) { this.Grade = "C+"; return "C+"; }
            else if (gradePoint < 69) { this.Grade = "B-"; return "B-"; }
            else if (gradePoint < 74) { this.Grade = "B"; return "B"; }
            else if (gradePoint < 79) { this.Grade = "B+"; return "B+"; }
            else if (gradePoint < 84) { this.Grade = "A-"; return "A-"; }
            else { this.Grade = "A"; return "A"; }
        }
    }
}
