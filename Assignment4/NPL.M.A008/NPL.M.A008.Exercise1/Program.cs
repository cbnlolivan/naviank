﻿using NPL.M.A008.Exercise;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.WriteLine("\tNPL.M.A008\n");


        List<Student> list = new List<Student>();
        //Perform Student object initialization using Optional Arguments
        list.Add(new());

        //Perform Student object initialization using Named Arguments
        list.Add(new(name: "Cong", @class: "Net 04", gender: "Male", entryDate: DateOnly.FromDayNumber(1), age: 20, address: ""));
        list.Add(new(name: "Linh", @class: "Net 05", gender: "Female", entryDate: DateOnly.FromDayNumber(2), age: 21, address: "", "Single"));
        list.Add(new(name: "Dung", @class: "Net 05", gender: "Male", entryDate: DateOnly.FromDayNumber(3), age: 20, address: "", "Complicated", 9));
        list.Add(new(name: "Manh", @class: "Net 05", gender: "Male", entryDate: DateOnly.FromDayNumber(4), age: 21, address: "", "Divorced", 8, "B"));
        //Implement ToString method with no parameter:
        Console.WriteLine("\nCall ToString Method with no parameter: ");
        Console.WriteLine(list[0].ToString());

        //Implement ToString method with tranmission parameter:
        Console.WriteLine("\nCall ToString Method with tranmission parameter: ");
        Console.WriteLine(list[1].ToString(name: list[1].Name, @class: list[1].Class, gender: list[1].Gender));
        Console.WriteLine(list[2].ToString(name: list[2].Name, @class: list[2].Class, gender: list[2].Gender, age: list[2].Age + "", relationship: list[2].Relationship));
        Console.WriteLine(list[3].ToString(name: list[3].Name, @class: list[3].Class, gender: list[3].Gender, grade: list[3].Grade, age: list[3].Age + ""));
        Console.WriteLine(list[4].ToString(name: list[4].Name, @class: list[4].Class, gender: list[4].Gender, grade: list[3].Grade, relationship: list[4].Relationship, age: list[4].Age + ""));

        //Implement the Graduate method call with transmission parameter:
        Console.WriteLine("\nCall Grade Method with transmission parameter: ");
        Console.WriteLine(list[1].Graduate(70M));
        Console.WriteLine(list[4].Graduate(91M));
        Console.WriteLine();

        ////Implement the Graduate method call with no parameter:
        Console.WriteLine("\nCall Grade Method with no parameter: ");
        Console.WriteLine(list[2].Graduate());
        Console.WriteLine(list[3].Graduate());
        Console.WriteLine();
        //foreach (Student student in list)
        //{
        //    Console.WriteLine(student.ToString(student.Name, student.Class, student.Grade, student.Relationship, student.Age + "", student.Grade));
        //}
    }
}