﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Employee
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public DateTime HiredDate { get; set; }
        public bool Status { get; set; }
        public int DepartmentID { get; set; }

        public Employee() { }
        public Employee(string id)
        {
            this.EmployeeID = id;
        }

        public Employee(string employeeID, string employeeName, int age, string address, DateTime hiredDate, bool status, int departmentID)
        {
            EmployeeID = employeeID;
            EmployeeName = employeeName;
            Age = age;
            Address = address;
            HiredDate = hiredDate;
            Status = status;
            DepartmentID = departmentID;
        }

        public override string? ToString()
        {
            return string.Format("{0,-8}{1,-32}{2,-8}{3,-40}{4,-20}{5,-8}{6, -8}",
                this.EmployeeID, this.EmployeeName, this.Age, this.Address, this.HiredDate, this.Status, this.DepartmentID);
        }
    }
}
