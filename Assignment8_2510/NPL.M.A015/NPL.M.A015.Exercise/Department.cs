﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Department
    {
        public int DepartmentID;
        public string DepartmentName;
        public Department() {
            DepartmentName = string.Empty;
        }

        public Department(int departmentID, string departmentName)
        {
            DepartmentID = departmentID;
            DepartmentName = departmentName;
        }

        public override string? ToString()
        {
            return string.Format("{0,16} {1,32}", this.DepartmentID, this.DepartmentName);
        }
    }
}
