﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace NPL.M.A015.Exercise
{
    internal class NDF
    {
        // Input functions
        public static T InputSomethingOfType<T>(string message, string errorMessage, string regex)
        {
            T result = default(T);
            while (true)
            {
                Console.Write(message);
                string tmp = Console.ReadLine();
                if (Regex.IsMatch(tmp, regex))
                {
                    try
                    {
                        Console.WriteLine();
                        result = (T)Convert.ChangeType(tmp, typeof(T));
                        return result;
                    }
                    catch (Exception ex) { }
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }
        }
        public static string InputstringOfRegexWithMessage(string message, string error, string regex)
        {
            while (true)
            {
                Console.Write(message);
                string res = Console.ReadLine().Trim();
                if (Regex.IsMatch(res, regex))
                {
                    Console.WriteLine();
                    return res;
                }
                else
                {
                    Console.WriteLine(error);
                }
            }
        }
        public static Boolean PressYNToContinue(string mess)
        {
            Boolean res = "Y".Equals(InputstringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
            return res;
        }
        public static int GetUserChoiceAsIntNumber(int lowerbound, int upperbound, string mess)
        {
            int res;
            while (true)
            {
                //Console.Write(mess);
                res = Convert.ToInt32(InputstringOfRegexWithMessage(mess, "Only digit characters accepted!", @"^\d+$"));
                if (res >= lowerbound && res <= upperbound)
                    return res;
                else
                    Console.WriteLine("Invalid choice! You'll have to input again!");
            }
        }
        public static string getNonEmptyString(string mess)
        {
            string ret = "";
            while (true)
            {
                Console.Write(mess);
                ret = Regex.Replace(Console.ReadLine(), "\\s+", " ");
                if (ret.Equals(""))
                {
                    Console.WriteLine("Please input Non-Empty string!!!");
                    continue;
                }
                return ret;
            }
        }
        public static string ReadLineOrCtrlEnter(string mess)
        {
            string retstring = "";
            string curLine = "";
            int curIndex = 0;
            Console.WriteLine(mess);
            do
            {
                ConsoleKeyInfo readKeyResult = Console.ReadKey(true);
                // handle Ctrl + Enter
                if (readKeyResult.Key == ConsoleKey.Enter && (readKeyResult.Modifiers & ConsoleModifiers.Control) != 0)
                {
                    if (!(curIndex == 0))
                    {
                        retstring += curLine;
                        Console.WriteLine();
                    }
                    return retstring;
                }
                // handle Enter
                if (readKeyResult.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    if (curIndex != 0)
                    {
                        retstring += curLine;
                        retstring += "\n";
                        curLine = "";
                    }
                    curIndex = 0;
                }
                // handle backspace
                else if (readKeyResult.Key == ConsoleKey.Backspace)
                {
                    if (curIndex > 0)
                    {
                        curLine = curLine.Remove(curLine.Length - 1);
                        Console.Write(readKeyResult.KeyChar);
                        Console.Write(' ');
                        Console.Write(readKeyResult.KeyChar);
                        curIndex--;
                    }
                }
                else
                // handle all other keypresses
                {
                    curLine += readKeyResult.KeyChar;
                    Console.Write(readKeyResult.KeyChar);
                    curIndex++;
                }
            }
            while (true);
        }
        public static List<string> InputMultipleLine(string mess)
        {
            Console.WriteLine(mess);
            List<string> res = new List<string>();
            string newLine = "";
            bool check = true;
            while (true)
            {
                newLine = Console.ReadLine();
                if (newLine != null && !newLine.Equals(""))
                {
                    if (!newLine.ToUpper().Contains("<END>"))
                    {
                        res.Add(newLine);
                    }
                    else
                    {
                        Match m = Regex.Match(newLine, "<END>");
                        string tmp = newLine.Substring(0, m.Index);
                        res.Add($"{tmp}");
                        break;
                    }
                }
            }

            return res;
        }
        // Data handling / manipulating functions
        public static List<string> DevideParagraphToList(string input)
        {
            List<string> list = new List<string>();
            string[] tmp = Regex.Split(input, @"\n+");
            foreach (string s in tmp)
            {
                list.Add(s);
            }
            return list;
        }
        public static string EncryptPassword(string password)
        {
            return password;
        }
        public static string NormalizeName(string name)
        {
            string res = "";
            string regex = "\\s+";
            string[] strings = name.Trim().Split(regex);
            string[] nameArr = strings;
            for (int i = 0; i < nameArr.Length; i++)
            {
                nameArr[i] = nameArr[i].ToLower();
                res += nameArr[i].Substring(0, 1) + nameArr[i].Substring(1);
                if (i < nameArr.Length) res += " ";
            }
            return res;
        }

        // Print out results in the specified format
        public static void PrintList<T>(List<T> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine();
        }
    }
}
