﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Manager
    {
        public List<ProgramingLanguage> LanguageList;
        public List<Employee> EmployeeList;
        public List<Department> DepartmentList;
        public List<(string, int)> EmployeeLanguage;

        public Manager()
        {
            this.LanguageList = new List<ProgramingLanguage>();
            this.EmployeeList = new List<Employee>();
            this.DepartmentList = new List<Department>();
            this.EmployeeLanguage = new List<(string, int)>();
        }

        public Manager(List<ProgramingLanguage> languageList, List<Employee> employeeList,
            List<Department> departmentList, List<(string, int)> employeeLanguage)
        {
            this.LanguageList = languageList;
            this.EmployeeList = employeeList;
            this.DepartmentList = departmentList;
            this.EmployeeLanguage = employeeLanguage;
        }

        // Data handling functions
        public Employee InputAnEmployee()
        {
            Employee res = new Employee();
            while (true)
            {
                string EmployeeID = NDF.InputstringOfRegexWithMessage("Employee ID (Like E01)", "Wrong format!", @"^E\d+$");
                int count = EmployeeList.Where(x => x.EmployeeID == res.EmployeeID).Select(x => x.EmployeeID).Count();
                if (count == 0)
                {
                    res.EmployeeID = EmployeeID;
                    break;
                }
                else
                {
                    Console.WriteLine("This employee has been existed!");
                }
            }
            res.EmployeeName = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Name: ", "Wrong name format!", @"^[a-zA-Z ]+$"));
            res.Age = NDF.InputSomethingOfType<int>("Age: ", "Wrong format", @"^\d+$");
            res.Address = NDF.getNonEmptyString("Address: ");
            res.HiredDate = DateTime.Parse(NDF.InputstringOfRegexWithMessage("Hired date (with format MM/dd/yyyy): ", "Wrong format", @"^.+$"));
            res.DepartmentID = NDF.InputSomethingOfType<int>("Department: ", "Wrong format", @"^\d+$");
            res.Status = NDF.InputstringOfRegexWithMessage("Is working (True/false): ", "Only true/false!", @"^(True|False){1}$").Equals("True");
            return res;
        }
        public void AddEmployee()
        {
            Console.WriteLine("\t====Add employees====");
            do
            {
                Employee tmp = InputAnEmployee();
                EmployeeList.Add(tmp);
            } while (NDF.PressYNToContinue("Add more employees? - Y or N: "));
        }

        public Department InputDepartment()
        {
            Department res = new Department();
            while (true)
            {
                int DepartmentID = NDF.InputSomethingOfType<int>("Employee ID (Like E01)", "Wrong format!", @"^\d+$");
                int count = DepartmentList.Where(x => x.DepartmentID == res.DepartmentID).Select(x => x.DepartmentID).Count();
                if (count == 0)
                {
                    res.DepartmentID = DepartmentID;
                    break;
                }
                else
                {
                    Console.WriteLine("This employee has been existed!");
                }
            }
            res.DepartmentName = NDF.getNonEmptyString("Department name");
            return res;
        }
        public void AddDepartment()
        {
            do
            {
                Department dep = InputDepartment();
                DepartmentList.Add(dep);
            } while (NDF.PressYNToContinue("Add more department? - Y or N: "));
        }
        // Get data functions
        public ProgramingLanguage GetLanguageByID(int idLanguage)
        {
            foreach (ProgramingLanguage language in LanguageList)
            {
                if (language.LanguageID == idLanguage) return language;
            }
            return null;
        }
        public ProgramingLanguage GetLanguageByName(string name)
        {
            foreach (ProgramingLanguage language in LanguageList)
            {
                if (language.LanguageName == name) return language;
            }
            return null;
        }

        public List<Department> GetDepartments(int numberOfEmployees)
        {
            List<Department> res = new List<Department>();
            var tmp = from dep in this.DepartmentList
                      join emp in this.EmployeeList on dep.DepartmentID equals emp.DepartmentID into DepGroup
                      select new
                      {
                          dep,
                          NumOfEmployees = DepGroup.Count(),
                      };
            foreach (var item in tmp)
            {
                if (item.NumOfEmployees >= numberOfEmployees) res.Add(item.dep);
            }
            return res;
        }

        public List<Employee> GetEmployeesWorking()
        {
            List<Employee> tmp = new List<Employee>();
            var res = (from emp in EmployeeList
                       where emp.Status == true
                       join dep in DepartmentList on emp.DepartmentID equals dep.DepartmentID
                       select new
                       {
                           Emp = emp,
                           Dep = dep
                       });
            foreach (var item in res)
            {
                Console.WriteLine(item);
            }

            return tmp;
        }
        public List<Employee> GetEmployees(string languageName)
        {
            List<Employee> res = new List<Employee>();
            ProgramingLanguage language = GetLanguageByName(languageName);

            var tmp = from emp in EmployeeList
                      join x in EmployeeLanguage on emp.EmployeeID equals x.Item1
                      where x.Item2 == language.LanguageID
                      select emp;
            foreach (var item in tmp)
            {
                Console.WriteLine(item);
            }
            return res;
        }

        public List<ProgramingLanguage> GetLanguages(string employeeId)
        {
            List<ProgramingLanguage> res = new List<ProgramingLanguage>();
            var tmp = from x in EmployeeLanguage
                      where x.Item1 == employeeId
                      group x by x.Item2 into AfterGroup
                      select new
                      {
                          Language = AfterGroup.Key,
                          NumOfEmps = AfterGroup.Count()
                      };
            foreach (var item in tmp)
            {
                if (item.NumOfEmps > 0)
                {
                    res.Add(GetLanguageByID(item.Language));
                }
            }
            return res;
        }
        public List<Employee> GetSeniorEmployee()
        {
            List<Employee> res = new List<Employee>();

            var tmp = from emp in EmployeeList
                      join x in EmployeeLanguage on emp.EmployeeID equals x.Item1 into AfterJoin
                      select new
                      {
                          emp,
                          NumOfLanguage = AfterJoin.Count()
                      };
            foreach (var item in tmp)
            {
                if (item.NumOfLanguage > 0) res.Add(item.emp);
            }
            return res;
        }

        public List<(Department, List<Employee>)> GetDepartments()
        {
            List<(Department, List<Employee>)> res = new List<(Department, List<Employee>)>();
            var tmp = from dep in DepartmentList
                      join emp in EmployeeList on dep.DepartmentID equals emp.DepartmentID into group1
                      from gr in group1.DefaultIfEmpty()
                      select new
                      {
                          dep,
                          Emp = gr/* != null ? gr : new Employee("E00")*/
                      };
            var tmp2 = from d in tmp
                       group d by d.dep into group2
                       select new
                       {
                           Depart = group2.Key,
                           Emps = group2
                       };
            Console.WriteLine(string.Format("{0,16} {1,32}", "DepartmentID", "DepartmentName"));
            foreach (var item in tmp2)
            {
                Console.WriteLine(item.Depart + "\n");
                int count = 1;
                foreach (var item2 in item.Emps)
                {
                    Console.WriteLine("\tEmployee " + count++ + ": " + item2.Emp);
                }
                Console.WriteLine();
            }
            return res;
        }

        public List<Employee> GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            var filteredEmployees = EmployeeList;
            if (!string.IsNullOrEmpty(employeeName))
            {
                filteredEmployees = filteredEmployees.Where(e => e.EmployeeName.Contains(employeeName)).ToList();
            }

            if (order == "ASC")
            {
                filteredEmployees = filteredEmployees.OrderBy(e => e.EmployeeName).ToList();
            }
            else if (order == "DESC")
            {
                filteredEmployees = filteredEmployees.OrderByDescending(e => e.EmployeeName).ToList();
            }

            int startIndex = (pageIndex - 1) * pageSize;

            var pagedEmployees = filteredEmployees.Skip(startIndex).Take(pageSize).ToList();

            return pagedEmployees;
        }
    }
}
