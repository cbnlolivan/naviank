﻿using NPL.M.A015.Exercise;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("======= LINQ Practice programm =======");

        List<ProgramingLanguage> LanguageList = new List<ProgramingLanguage>
        {
            new ProgramingLanguage(1, "C"),
            new ProgramingLanguage(2, "C++"),
            new ProgramingLanguage(3, "Java"),
            new ProgramingLanguage(4, "C#")
        };
        List<Department> DepartmentList = new List<Department>
        {
            new Department(1, "Department 1"),
            new Department(2, "Department 2"),
            new Department(3, "Department 3"),
            new Department(4, "Department 4"),
            new Department(5, "Department 5")
        };
        List<Employee> EmployeeList = new List<Employee>
        {
            new Employee("E01", "Pham Khai Van", 21, "Thanh pho Bac Ninh", DateTime.Now, true, 1),
            new Employee("E02", "Tran Hong Ngoc", 21, "Ha Noi", DateTime.Now, true, 2),
            new Employee("E03", "Le Thuy Dung", 21, "Yen Phong", DateTime.Now, false, 3),
            new Employee("E04", "Tran Ha Ngoc Thao", 21, "Thanh pho Bac Ninh", DateTime.Now, true, 5),
            new Employee("E05", "Ngo Thi Thanh Lam", 21, "Tien Du", DateTime.Now, true, 3),
        };
        List<(string, int)> EmployeeLanguage = new List<(string, int)>
        {
            ("E01", 1), //C
            ("E01", 3), //Java
            ("E01", 4), //C#
            ("E03", 1), //C
            ("E03", 2), //C++
            ("E03", 3), //Java
            ("E04", 1), //C
            ("E04", 2)  //C++
        };

        Manager manager = new Manager(LanguageList, EmployeeList, DepartmentList, EmployeeLanguage);

        int choice;
        do
        {
            Console.WriteLine("\tMenu options:");
            Console.WriteLine("1. Print out departments which have the number of employees greater than or equal an integer.");
            Console.WriteLine("2. Get list of all employees are working.");
            Console.WriteLine("3. Get list of all employees know this programing language.");
            Console.WriteLine("4. Get languages that an employee knows");
            Console.WriteLine("5. Get list of employees who know multiple programming languages.");
            Console.WriteLine("6. Get Employee Paging.");
            Console.WriteLine("7. Get all departments including employees that belong to each department.");
            Console.WriteLine("8. ");
            Console.WriteLine("9. ");
            Console.WriteLine("10. ");
            Console.WriteLine("11. ");
            Console.WriteLine("0. Exit");
            choice = NDF.GetUserChoiceAsIntNumber(0, 12, "Choose option an integer: ");
            switch (choice)
            {
                case 0:
                    Console.WriteLine("Exit program!");
                    break;
                case 1:
                    int paramFunction1 = NDF.InputSomethingOfType<int>("Enter an integer: ", "Wrong format", @"^\d+$");
                    NDF.PrintList<Department>(manager.GetDepartments(paramFunction1));
                    break;
                case 2:
                    NDF.PrintList<Employee>(manager.GetEmployeesWorking());
                    break;
                case 3:
                    string paramFunction3 = NDF.getNonEmptyString("Input language name: ");
                    manager.GetEmployees(paramFunction3);
                    break;
                case 4:
                    string paramFunction4 = NDF.getNonEmptyString("Enter employee ID: ");
                    NDF.PrintList<ProgramingLanguage>(manager.GetLanguages(paramFunction4));
                    break;
                case 5:
                    NDF.PrintList<Employee>(manager.GetSeniorEmployee());
                    break;
                case 6:
                    int index = NDF.InputSomethingOfType<int>("Page index: ", "Wrong format", @"^\d+%");
                    int size = NDF.InputSomethingOfType<int>("Page size: ", "Wrong format", @"^\d+%");
                    string name = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Employee name: ", "Wrong format!", @"^[a-zA-Z ]+$"));
                    string odo= NDF.InputstringOfRegexWithMessage("Is working (True/false): ", "Only true/false!", @"^(ASC|DESC){1}$");
                    NDF.PrintList<Employee>(manager.GetEmployeePaging(pageIndex: index, pageSize: size, employeeName: name, order: odo));
                    break;
                case 7:
                    manager.GetDepartments();
                    break;
                case 8:
                    Console.WriteLine();
                    break;
                case 9:
                    Console.WriteLine();
                    break;

            }
        } while (choice != 0);

    }
}