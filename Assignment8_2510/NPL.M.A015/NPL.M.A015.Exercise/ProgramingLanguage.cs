﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class ProgramingLanguage
    {
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public ProgramingLanguage() {
            this.LanguageName = string.Empty;
        }

        public ProgramingLanguage(int iD, string languageName)
        {
            LanguageID = iD;
            LanguageName = languageName;
        }

        public override string ToString()
        {
            return string.Format("{0, -8} {1, -20}", this.LanguageID, this.LanguageName);
        }
    }
}
