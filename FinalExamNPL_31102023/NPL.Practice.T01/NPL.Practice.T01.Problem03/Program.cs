﻿using NPL.Practice.T01.Problem03;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("\tProblem 3");
        PhoneBookManagement manager = new PhoneBookManagement();
        manager.ListPhoneBook = InitPhoneBook();

        int choice;
        do
        {
            Console.WriteLine("Option menu:");
            Console.WriteLine("\t1. View phone book list");
            Console.WriteLine("\t2. Add new phone book");
            Console.WriteLine("\t3. Remove a phone book has name");
            Console.WriteLine("\t4. Find phone book by name");
            Console.WriteLine("\t5. Display the sorted list");
            Console.WriteLine("\t0. Exit");
            choice = NDF.GetUserChoiceAsIntNumber(0, 5, "Select an option as an integer: ");
            switch (choice)
            {
                case 0:
                    Console.WriteLine("Exit program!");
                    break;
                case 1:
                    manager.PrintList();
                    break;
                case 2:
                    manager.Add();
                    break;
                case 3:
                    manager.Remove(NDF.InputstringOfRegexWithMessage("Input name for removing: ",
                        "Name format is not correct!", @"^[a-zA-Z ]+$"));
                    break;
                case 4:
                    NDF.PrintList<PhoneBook>(manager.Find(NDF.InputstringOfRegexWithMessage("Input name for searching: ",
                        "Name format is not correct!", @"^[a-zA-Z ]+$")), "\tFound list:\n" +
                        string.Format("{0,-24}{1,-18}{2,-24}{3,-48}{4,-12}",
                        "Name", "PhoneNumber", "Email", "Address", "Group"));
                    break;
                case 5:
                    NDF.PrintList<PhoneBook>(manager.Sort(), "\tSorted list:\n" +
                        string.Format("{0,-24}{1,-18}{2,-24}{3,-48}{4,-12}",
                        "Name", "PhoneNumber", "Email", "Address", "Group"));
                    break;
            }
        } while (choice != 0);
    }
    private static List<PhoneBook> InitPhoneBook()
    {
        return new List<PhoneBook>
            {
                new PhoneBook("Pham Khai Van", "038 447 8185", "cbnlolivan@gmail.com", "Bac Ninh", PhoneGroup.Family),
                new PhoneBook("Tran Hong Ngoc", "038 447 8186", "thn@gmail.com", "Ha Noi", PhoneGroup.Colleage),
                new PhoneBook("Le Thuy Dung", "038 447 8187", "ltd@gmail.com", "Bac Ninh", PhoneGroup.Other),
                new PhoneBook("Do Thuy Linh", "038 447 8188", "dtl@gmail.com", "Bac Ninh", PhoneGroup.Other),
                new PhoneBook("Pham Minh Tam", "038 447 8189", "pmt@gmail.com", "Bac Ninh", PhoneGroup.Family)
            };
    }
}