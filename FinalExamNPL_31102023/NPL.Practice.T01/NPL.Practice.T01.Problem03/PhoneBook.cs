﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T01.Problem03
{
    internal class PhoneBook : IComparable<PhoneBook>
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public PhoneGroup Group { get; set; }

        public PhoneBook()
        {
        }

        public PhoneBook(string name, string phoneNumber, string email, string address, PhoneGroup group)
        {
            Name = name;
            PhoneNumber = phoneNumber;
            Email = email;
            Address = address;
            Group = group;
        }

        public override string? ToString()
        {
            return string.Format("{0,-24}{1,-18}{2,-24}{3,-48}{4,-12}", 
                this.Name, this.PhoneNumber, this.Email, this.Address, this.Group);
        }

        public int CompareTo(PhoneBook? other)
        {
            return this.Name.CompareTo(other.Name);
        }


    }
}
