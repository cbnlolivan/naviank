﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T01.Problem03
{
    internal class PhoneBookManagement
    {
        public List<PhoneBook> ListPhoneBook { get; set; }

        public void PrintList()
        {
            if (ListPhoneBook == null || ListPhoneBook.Count == 0)
            {
                Console.WriteLine("There is no phone book in the list now!");
                return;
            }
            Console.WriteLine(string.Format("{0,-24}{1,-18}{2,-24}{3,-48}{4,-12}",
                "Name", "PhoneNumber", "Email", "Address", "Group"));
            for (int i = 0; i < ListPhoneBook.Count; i++)
            {
                Console.WriteLine(ListPhoneBook[i]);
            }
        }
        public PhoneBook InputAPhoneBook()
        {
            Console.WriteLine("Input a new phone book:");
            PhoneBook phoneBook = new PhoneBook();
            phoneBook.Name = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Name: ",
                "Name must contain only alphabetical characters", @"^[a-zA-Z ]+$"));
            while (true)
            {
                string phoneNumber = NDF.InputPhoneNumber("Phone number (as format 0## #### ####): ");
                int x = this.ListPhoneBook.Where(x => x.PhoneNumber.Equals(phoneNumber)).Count();
                if (x > 0)
                {
                    Console.WriteLine("This phone number has existed in the list!");
                }
                else
                {
                    phoneBook.PhoneNumber = phoneNumber;
                    break;
                }
            }
            phoneBook.Email = NDF.InputNormalEmail("Email: ");
            phoneBook.Address = NDF.InputstringOfRegexWithMessage("Address: ",
                "Address is not correct!", @"^[a-zA-Z0-9 ]+$");
            string phoneGroupValues = "Phone group must be one of the following values:\n" +
                "1. Family\n2. Colleage\n3. Friend\n4. Other\nPhone group number: ";
            phoneBook.Group = (PhoneGroup)NDF.GetUserChoiceAsIntNumber(0, 3, phoneGroupValues);
            return phoneBook;
        }
        public void Add()
        {
            int count = 0;
            do
            {
                PhoneBook tmp = InputAPhoneBook();
                if (!this.ListPhoneBook.Contains(tmp))
                {
                    ListPhoneBook.Add(tmp);
                    count++;
                }
            } while (NDF.PressYNToContinue("Add new phone to phone book? Y or N: "));
            Console.WriteLine("Added " + count + (count > 1 ? "elements" : "element") + "to the list");
        }
        public void Remove(string phoneBookName)
        {
            var foundList = this.ListPhoneBook.Where(x => x.Name.ToLower().Contains(phoneBookName.ToLower())).ToList();
            if (foundList.Count > 0)
            {
                foreach (var phoneBook in foundList)
                {
                    this.ListPhoneBook.Remove(phoneBook);
                }
                Console.WriteLine("Removed " + foundList.Count + " element from phone book list!");
            }
            else
            {
                Console.WriteLine("Not found any phone has name = " + phoneBookName);
            }
        }
        public List<PhoneBook> Sort()
        {
            return this.ListPhoneBook.OrderBy(x => x.Name).ToList();
        }

        public List<PhoneBook> Find(string phoneBookName)
        {
            List<PhoneBook> res;
            res = this.ListPhoneBook.Where(x => x.Name.ToLower().Contains(phoneBookName.ToLower())).ToList();

            return res;
        }
    }
}
