﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.Practice.T01.Problem03
{
    internal class NDF
    {
        public static string InputstringOfRegexWithMessage(string message, string error, string regex)
        {

            Console.Write(message);
            string res = Console.ReadLine().Trim();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine();
                return res;
            }
            else
            {
                throw new Exception(error);
            }
        }
        public static string NormalizeName(string name)

        {
            string res = "";
            string regex = "\\s+";
            string[] strings = Regex.Split(name, regex);
            string[] nameArr = strings;
            for (int i = 0; i < nameArr.Length; i++)
            {
                nameArr[i] = nameArr[i].ToLower();
                res += nameArr[i].Substring(0, 1).ToUpper() + nameArr[i].Substring(1);
                if (i < nameArr.Length) res += " ";
            }
            return res;
        }
        public static string InputNormalEmail(string mess)
        {
            string regex = @"^[A-Za-z]([^@]*)(?<![-.])([@]{1})(?![.-])[a-zA-Z0-9-.]+(?<![-.])$";
            string email = InputstringOfRegexWithMessage(mess, "Email is not correct!", regex);
            return email;
        }
        public static string InputPhoneNumber(string mess)
        {
            string regex = @"^0[\d]{2} \d{3} \d{4}$";
            string phoneNum = InputstringOfRegexWithMessage(mess, "Phone number is not correct!!", regex);
            return phoneNum;
        }
        public static int GetUserChoiceAsIntNumber(int lowerbound, int upperbound, string mess)
        {
            int res;
            //Console.Write(mess);
            res = Convert.ToInt32(InputstringOfRegexWithMessage(mess, "Only digit characters accepted!", @"^\d+$"));
            if (res >= lowerbound && res <= upperbound)
                return res;
            else
                throw new Exception("Phone group is not correct!");
        }
        public static Boolean PressYNToContinue(string mess)
        {
            Boolean res = "Y".Equals(InputstringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
            return res;
        }
        public static void PrintList<T>(List<T> list, string mess)
        {
            if (list == null || list.Count == 0)
            {
                Console.WriteLine("There is no item in the list now!");
                return;
            }
            Console.WriteLine(mess + "\n");
            foreach (var item in list)
            {
                Console.WriteLine(item?.ToString() + "\n");
            }
            Console.WriteLine();
        }
    }
}
