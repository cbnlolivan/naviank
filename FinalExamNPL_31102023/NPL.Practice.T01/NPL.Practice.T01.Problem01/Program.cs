﻿using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("\tProblem 1\n");
        int length = InputSomethingOfType<int>("Input array length (an integer): ", "Wrong integer format!", @"^[+-]?[\s\d]+$");
        int[] chartInput = new int[length];
        for (int i = 0; i < length; i++)
        {
            chartInput[i] = InputSomethingOfType<int>("Array element at index " + i + ": ", "Wrong integer format!", @"^[+-]?[\s\d]+$"); ;
        }
        Console.WriteLine("\n=============");
        //Console.WriteLine(typeof(decimal).IsPrimitive);
        PrintNumericArray<int>(chartInput, "Input:");
        Console.WriteLine();
        PrintNumericArray<decimal>(DrawCircleChart(chartInput), "Output:");
    }
    private static decimal[] DrawCircleChart(int[] chartInput)
    {
        decimal[] res = new decimal[chartInput.Length];
        int intSum = chartInput.Sum(x => x);
        decimal decimalSum = 0;
        for (int i = 0; i < chartInput.Length - 1; i++)
        {
            res[i] = Math.Round((decimal)100 * chartInput[i] / intSum, 2);
            decimalSum += res[i];
        }
        res[chartInput.Length - 1] = 100 - decimalSum;
        return res;
    }
    private static T InputSomethingOfType<T>(string message, string errorMessage, string regex)
    {
        T result = default(T);
        while (true)
        {
            Console.Write(message);
            string tmp = Console.ReadLine();
            if (tmp.Equals(""))
            {
                Console.WriteLine("This field must not be empty!");
                continue;
            }
            if (Regex.IsMatch(tmp, regex))
            {
                try
                {
                    Console.WriteLine();
                    result = (T)Convert.ChangeType(tmp, typeof(T));
                    return result;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Can not parse input to data type " + typeof(T));
                }
            }
            else
            {
                Console.WriteLine(errorMessage);
            }
        }
    }
    private static void PrintNumericArray<T>(T[] array, string mess)
    {
        Type type = typeof(T);
        if ((type.IsPrimitive && type != typeof(bool) && type != typeof(char)) || type == typeof(decimal))
        {
            Console.Write(mess);
            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0)
                    Console.Write("{" + Math.Round(Convert.ToDecimal(array[i]), 2));
                else if (i == array.Length - 1)
                    Console.Write(", " + Math.Round(Convert.ToDecimal(array[i]), 2) + "}");
                else
                    Console.Write(", " + Math.Round(Convert.ToDecimal(array[i]), 2));
            }
        }
        else
        {
            Console.WriteLine("The array type is not a nummeric type!!!");
        }
    }
}