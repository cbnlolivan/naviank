﻿using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("\tProblem2\n");
        List<string> listEmployees = GetListOfStrings();

        Console.WriteLine("Input names:");
        foreach (string employee in listEmployees)
        {
            if (!employee.Equals(""))
                Console.WriteLine(employee);
        }

        Console.WriteLine("\nOutput email addresses:");
        List<string> res = GenerateEmailAddress(listEmployees);
        foreach (string employee in res)
        {
            Console.WriteLine(employee);
        }
    }

    public static string ShortenName(string name)
    {
        string res = "";
        string[] nameElement = Regex.Split(name, "\\s+");
        res = nameElement[nameElement.Length - 1].ToLower();
        for (int i = 0; i < nameElement.Length - 1; i++)
        {
            res += nameElement[i].Substring(0, 1).ToLower();
        }
        return res;
    }
    static private List<string> GenerateEmailAddress(List<string> listEmployees)
    {
        List<string> result = new List<string>();
        foreach (string employee in listEmployees)
        {
            if (employee == null || employee.Equals(""))
                continue;
            string shortenedName = ShortenName(employee);
            string mailAddress = shortenedName + "@fsoft.com.vn";
            int i = 1;
            while (true)
            {
                if (result.Contains(mailAddress))
                {
                    mailAddress = shortenedName + i + "@fsoft.com.vn";
                }
                else
                {
                    result.Add(mailAddress);
                    break;
                }
            }
        }
        return result;
    }

    public static string NormalizeName(string name)
    {
        string res = "";
        string regex = "\\s+";
        string[] strings = name.Trim().Split(regex);
        string[] nameArr = strings;
        for (int i = 0; i < nameArr.Length; i++)
        {
            nameArr[i] = nameArr[i].ToLower();
            res += nameArr[i].Substring(0, 1).ToUpper() + nameArr[i].Substring(1);
            if (i < nameArr.Length) res += " ";
        }
        return res;
    }
    public static List<string> GetListOfStrings()
    {
        List<string> res = new List<string>();

        string mess = "Input list of employee names (in the order: first name + middle name + last name)." +
            "Each name in a line. Press \"Enter\n to stop!\nInput: ";
        Console.WriteLine(mess);
        string regex = @"^([a-zA-Z ])+$";
        while (true)
        {
            string tmp = InputstringOfRegexWithMessage("", "Wrong format!", regex);
            if (tmp == null || tmp.Equals(""))
            {
                break;
            }
            res.Add(tmp.Trim());
        }

        //string[] array = listNameString.Split(',');
        //foreach (string str in array)
        //{
        //    if (str == null || str.Equals(""))
        //        continue;
        //    else if (!res.Contains(str.Trim()))
        //        res.Add(str.Trim());
        //}
        return res;
    }
    public static string InputstringOfRegexWithMessage(string message, string error, string regex)
    {
        while (true)
        {
            Console.Write(message);
            string res = Console.ReadLine().Trim();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine("Accepted input: " + res);
                //Console.WriteLine();
                return res;
            }
            else
            {
                Console.WriteLine(error);
            }
        }
    }
}