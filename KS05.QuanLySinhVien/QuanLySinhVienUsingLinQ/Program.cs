﻿
using QuanLySinhVienUsingLinQ;
using System.Text;

QuanLy quanLySinhVien = new QuanLy();

Console.OutputEncoding = Encoding.Unicode;
int luaChon;
do
{
    Console.WriteLine("Chương trình quản lý sinh viên");
    Console.WriteLine("1. Thêm sinh viên.");
    Console.WriteLine("2. Cập nhật thông tin sinh viên bởi mã.");
    Console.WriteLine("3. Xóa sinh viên bởi mã.");
    Console.WriteLine("4. Tìm kiếm sinh viên theo tên.");
    Console.WriteLine("5. Sắp xếp sinh viên theo điểm trung bình (GPA).");
    Console.WriteLine("6. Sắp xếp sinh viên theo tên.");
    Console.WriteLine("7. Sắp xếp sinh viên theo mã.");
    Console.WriteLine("8. Hiển thị danh sách sinh viên.");
    Console.WriteLine("0. Thoát.");

    Console.WriteLine("Mời bạn nhập lựa chọn: ");
    luaChon = int.Parse(Console.ReadLine());

    switch (luaChon)
    {
        case 1:
            quanLySinhVien.ThemSinhVien();
            break;
        case 2:
            quanLySinhVien.CapNhatSinhVien();
            break;
        case 3:
            quanLySinhVien.XoaSinhVien();
            break;
        case 4:
            quanLySinhVien.TimKiemTheoTen();
            break;
        case 5:
            quanLySinhVien.SapXepTheoTrungBinh();
            break;
        case 6:
            quanLySinhVien.SapXepTheoTen();
            break;
        case 7:
            break;
        case 8:
            quanLySinhVien.HienThiDanhSachSinhVien();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;

    }

}
while (luaChon > 0 && luaChon <= 8);
