﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVienUsingLinQ
{
    internal class SinhVien
    {
        public int ma { get; set; }
        public string tenSinhVien { get; set; }
        public string gioiTinh { get; set; }
        public int tuoi { get; set; }
        public float diemToan { get; set; }
        public float diemLy { get; set; }
        public float diemHoa { get; set; }

        public SinhVien() { }

        public SinhVien(
            int ma,
            string tenSinhVien,
            string gioiTinh,
            int tuoi,
            float diemToan,
            float diemLy,
            float diemHoa
            )
        {
            this.ma = ma;
            this.tenSinhVien = tenSinhVien;
            this.gioiTinh = gioiTinh;
            this.tuoi = tuoi;
            this.diemToan = diemToan;
            this.diemLy = diemLy;
            this.diemHoa = diemHoa;
        }

    }
}
