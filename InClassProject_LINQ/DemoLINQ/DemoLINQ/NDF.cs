﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace NPL.M.A010.Exercise
{
    internal class NDF
    {
        // Input functions
        public static T InputSomethingOfType<T>(string message, string errorMessage, string regex)
        {
            T result = default(T);
            while (true)
            {
                Console.Write(message);
                string tmp = Console.ReadLine();
                if (Regex.IsMatch(tmp, regex))
                {
                    try
                    {
                        Console.WriteLine("Accepted input:" + tmp);
                        Console.WriteLine();
                        result = (T)Convert.ChangeType(tmp, typeof(T));
                        return result;
                    }
                    catch (Exception ex) { }
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }
        }
        public static string InputstringOfRegexWithMessage(string message, string error, string regex)
        {
            while (true)
            {
                Console.Write(message);
                string res = Console.ReadLine().Trim();
                if (Regex.IsMatch(res, regex))
                {
                    Console.WriteLine("Accepted input:" + res);
                    Console.WriteLine();
                    return res;
                }
                else
                {
                    Console.WriteLine(error);
                }
            }
        }
        public static DateTime InputBirthDate(string mess)
        {
            string TimeRaw = InputstringOfRegexWithMessage("Enter date (follow the format dd/MM/yyyy): ", "Wrong date format!!!", @"^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$");
            string[] TimeArr = TimeRaw.Split("/");
            int Date = Int32.Parse(TimeArr[0]);
            int Month = Int32.Parse(TimeArr[1]);
            int Year = Int32.Parse(TimeArr[2]);
            DateTime result = new DateTime(Year, Month, Date);
            return result;
        }
        public static string InputNormalEmail(string mess)
        {
            string regex = @"^[A-Za-z]([^@]*)(?<![-.])([@]{1})(?![.-])[a-zA-Z0-9-.]+(?<![-.])$";
            string email = InputstringOfRegexWithMessage(mess, "Please enter email with format <account name>@<domain>", regex);
            return email;
        }
        public static string InputFsoftMail(string mess)
        {
            string regex = @"^([A-Za-z]([^@]*)(?<![-.])@fsoft.com.vn$)";
            string email = InputstringOfRegexWithMessage(mess, "Please enter email with format <account name>@fsoft.com.vn", regex);
            return email;
        }
        public static List<string> InputMultipleFsoftMail(string mess)
        {
            List<string> list = new List<string>();
            string regex = @"^([A-Za-z][^@]*(?<![-.])@fsoft.com.vn(?:\s*,\s*)?)+$";
            string email = InputstringOfRegexWithMessage(mess, "Please enter email with format <account name>@fsoft.com.vn", regex);
            string[] arr = Regex.Split(email, "\\s*,\\s*");
            foreach (string item in arr)
            {
                if (!list.Contains(item))
                    list.Add(item);
            }
            return list;
        }
        public static string InputPhoneNumber(int minLength, string mess)
        {
            string regex = @"^[0-9]{" + minLength + @",}$";
            string phoneNum = InputstringOfRegexWithMessage(mess, "Invalid phone number!!", regex).Replace("\\s+", "");
            return phoneNum;
        }
        public static Boolean PressYNToContinue(string mess)
        {
            Boolean res = "Y".Equals(InputstringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
            return res;
        }
        public static int GetUserChoiceAsIntNumber(int lowerbound, int upperbound, string mess)
        {
            int res;
            while (true)
            {
                //Console.Write(mess);
                res = Convert.ToInt32(InputstringOfRegexWithMessage(mess, "Only digit characters accepted!", @"^\d+$"));
                if (res >= lowerbound && res <= upperbound)
                    return res;
                else
                    Console.WriteLine("Invalid choice! You'll have to input again!");
            }
        }
        public static string getNonEmptyString(string mess)
        {
            string ret = "";
            while (true)
            {
                Console.Write(mess);
                ret = Regex.Replace(Console.ReadLine(), "\\s+", " ");
                if (ret.Equals(""))
                {
                    Console.WriteLine("Please input Non-Empty string!!!");
                    continue;
                }
                return ret;
            }
        }
        public static string ReadLineOrCtrlEnter(string mess)
        {
            string retstring = "";
            string curLine = "";
            int curIndex = 0;
            Console.WriteLine(mess);
            do
            {
                ConsoleKeyInfo readKeyResult = Console.ReadKey(true);
                // handle Ctrl + Enter
                if (readKeyResult.Key == ConsoleKey.Enter && (readKeyResult.Modifiers & ConsoleModifiers.Control) != 0)
                {
                    //Console.WriteLine();
                    return retstring;
                }
                // handle Enter
                if (readKeyResult.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    if (curIndex != 1)
                    {
                        //if (GetDateTimeInstring(curLine) != DateTime.Now.AddDays(1))
                        retstring += curLine;
                        retstring += "\n";
                        curLine = "";
                    }
                    curIndex = 0;
                }
                // handle backspace
                if (readKeyResult.Key == ConsoleKey.Backspace)
                {
                    if (curIndex > 0)
                    {
                        retstring = retstring.Remove(retstring.Length - 1);
                        Console.Write(readKeyResult.KeyChar);
                        Console.Write(' ');
                        Console.Write(readKeyResult.KeyChar);
                        curIndex--;
                    }
                }
                else
                // handle all other keypresses
                {
                    curLine += readKeyResult.KeyChar;
                    Console.Write(readKeyResult.KeyChar);
                    curIndex++;
                }
                //Console.WriteLine(curIndex);
            }
            while (true);
        }
        // Data handling / manipulating functions
        public static string EncryptPassword(string password)
        {
            return password;
        }
        public static string NormalizeName(string name)
        {
            string res = "";
            string regex = "\\s+";
            string[] strings = name.Trim().Split(regex);
            string[] nameArr = strings;
            for (int i = 0; i < nameArr.Length; i++)
            {
                nameArr[i] = nameArr[i].ToLower();
                res += nameArr[i].Substring(0, 1).ToUpper() + nameArr[i].Substring(1);
                if (i < nameArr.Length) res += " ";
            }
            return res;
        }

        //public static void Main(string[] args)
        //{
        //    string res = NDF.ReadLineOrCtrlEnter("Input: ");
        //    Console.WriteLine(res);
        //}
    }

}
