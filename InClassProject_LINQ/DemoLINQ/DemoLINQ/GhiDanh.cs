﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DemoLINQ
{
    internal class GhiDanh
    {
        public int MaSinhVien { get; set; }
        public int MaMonHoc { get; set; }
        public float Diem { get; set; }

        public GhiDanh() { }

        public GhiDanh(int maSinhVien, int maMonHoc)
        {
            MaSinhVien = maSinhVien;
            MaMonHoc = maMonHoc;
        }

        public GhiDanh(int maSinhVien, int maMonHoc, float diem) : this(maSinhVien, maMonHoc)
        {
            Diem = diem;
        }


    }
}
