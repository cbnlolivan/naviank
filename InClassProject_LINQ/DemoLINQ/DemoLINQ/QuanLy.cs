﻿using NPL.M.A010.Exercise;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DemoLINQ
{
    internal class QuanLy
    {
        public List<SinhVien> listSV { get; set; }
        public List<MonHoc> listMH { get; set; }
        public List<GhiDanh> listGD { get; set; }

        public QuanLy(List<SinhVien> listSV, List<MonHoc> listMH, List<GhiDanh> listGD)
        {
            this.listSV = listSV;
            this.listMH = listMH;
            this.listGD = listGD;
        }

        public SinhVien ChonSinhVienCoMaSV(int maSV)
        {
            foreach (SinhVien sv in listSV)
            {
                if (sv.MaSinhVien == maSV) return sv;
            }
            return null;
        }
        public MonHoc ChonMonHocCoMaMH(int maMH)
        {
            foreach (MonHoc sv in listMH)
            {
                if (sv.MaMonHoc == maMH) return sv;
            }
            return null;
        }

        public SinhVien Nhap1SinhVienMoi()
        {
            SinhVien res = new SinhVien();
            int maSV = NDF.InputSomethingOfType<int>("Ma sinh vien: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            if (ChonSinhVienCoMaSV(maSV) != null)
            {
                Console.WriteLine("Ma sinh vien nay da ton tai roi nhe!");
                return null;
            }
            res.MaSinhVien = maSV;
            res.HoTen = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Ten sinh vien: ", "Sai dinh dang roi, nhap lai di.", @"^[a-zA-Z ]+$"));
            res.Tuoi = NDF.InputSomethingOfType<int>("Tuoi: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            res.Khoa = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Khoa: ", "Sai dinh dang roi, nhap lai di.", @"^[a-zA-Z0-9 ]+$"));

            return res;
        }
        public void NhapNhieuSinhVienMoi()
        {
            do
            {
                listSV.Add(Nhap1SinhVienMoi());
            } while (NDF.PressYNToContinue("Nhập thêm sinh viên không nhỉ?\nGõ y hoặc Y để tiếp tục: "));
        }
        public MonHoc Nhap1MonHocMoi()
        {
            MonHoc res = new MonHoc();
            int maMH = NDF.InputSomethingOfType<int>("Ma mon hoc: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            if (ChonMonHocCoMaMH(maMH) != null)
            {
                Console.WriteLine("Ma mon hoc nay da ton tai roi nhe!");
                return null;
            }
            res.MaMonHoc = maMH;
            res.TenMonHoc = NDF.InputSomethingOfType<string>("Ten mon hoc: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            return res;
        }
        public void NhapNhieuMonHocMoi()
        {
            do
            {
                listMH.Add(Nhap1MonHocMoi());
            } while (NDF.PressYNToContinue("Nhập thêm môn học mới không nhỉ!? : "));
        }
        public void GhiDanh1SinhVien()
        {
            int maSV = NDF.InputSomethingOfType<int>("Ma sinh vien: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            int maMH = NDF.InputSomethingOfType<int>("Ma mon hoc: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            int checkDuyNhat = listGD
                                .Where(x => (x.MaMonHoc == maMH) && (x.MaSinhVien == maSV))
                                .Count();
            if (checkDuyNhat == 1)
            {
                Console.WriteLine("Sinh viên có mã " + maSV + " đã đăng ký môn học " + maMH + " !");
                return;
            }
            if (ChonSinhVienCoMaSV(maSV) == null)
            {
                SinhVien s = new SinhVien();
                s.MaSinhVien = maSV;
                Console.WriteLine("Sinh viên với mã " + maSV + " chưa có trong danh sách. Vui lòng nhập thông tin cho nó.");
                s.NhapThongTin(cMa: false);
                listSV.Add(s);
            }
            if (ChonMonHocCoMaMH(maMH) == null)
            {
                MonHoc m = new MonHoc();
                m.MaMonHoc = maMH;
                Console.WriteLine("Môn học với mã " + maMH + " chưa có trong danh sách. Vui lòng nhập thông tin cho nó.");
                m.NhapThongTin(cMa: false);
                listMH.Add(m);
            }

            float diem = NDF.InputSomethingOfType<float>("Diem: ", "Sai dinh dang roi, nhap lai di.", @"^-?\d+\.?\d*$");
            GhiDanh res = new GhiDanh(maSV, maMH, diem);
            if (!listGD.Contains(res))
                listGD.Add(res);
            else
            {
                Console.WriteLine("");
            }
        }

        public void InDanhSachMonHoc()
        {
            Console.WriteLine("\n\tDanh sach mon hoc: ");
            var dsMonHoc = from m in listMH
                           select m.TenMonHoc;
            foreach (var d in dsMonHoc)
            {
                Console.WriteLine(d);
            }
        }
        public void InDanhSachSinhVien()
        {
            Console.WriteLine("\n\tDanh sach sinh vien: ");
            var dsSinhVien = from s in listSV
                             select new { s.MaSinhVien, s.HoTen };
            foreach (var item in dsSinhVien)
            {
                Console.WriteLine(item.MaSinhVien + ": " + item.HoTen);
            }
        }
        public void InDanhSachGhiDanh()
        {
            Console.WriteLine("\n\tDanh sach ghi danh");
            var thongtinSinhVienQuery2 = from sv in listSV
                                         join gd in listGD on sv.MaSinhVien equals gd.MaSinhVien
                                         join mh in listMH on gd.MaMonHoc equals mh.MaMonHoc
                                         orderby gd.MaMonHoc descending
                                         select (sv.HoTen, mh.TenMonHoc, gd.Diem);
            Console.WriteLine(string.Format("{0, -30} {1, -30} {2, -8}", "TenMonHoc", "HoTen", "Diem"));
            foreach (var item in thongtinSinhVienQuery2)
            {
                Console.WriteLine(string.Format("{0, -30} {1, -30} {2,-8}", item.TenMonHoc, item.HoTen, item.Diem));
            }
        }

        public void InSoLuongSinhVien()
        {
            var res = (from sv in listSV
                       select sv).ToList().Count;
            Console.WriteLine("\nSo luong sinh vien: " + res);
        }
        public void InDanhSachSinhVienVaSoMonHoc()
        {
            Console.WriteLine("\n\tDanh sach sinh vien va so mon hoc cua sinh vien do: ");
            var tmp = from sv in listSV
                      join gd in listGD on sv.MaSinhVien equals gd.MaSinhVien
                      into table
                      select new
                      {
                          TenSinhVien = sv.HoTen,
                          SoMonHoc = table.Count()
                      };
            var tmp2 = from sv in listSV
                      join gd in listGD on sv.MaSinhVien equals gd.MaSinhVien
                      into table
                      from n in table.DefaultIfEmpty()
                      select new
                      {
                          TenSinhVien = sv.HoTen,
                          SoMonHoc = table.Count()
                      };

            foreach (var item in tmp)
            {
                Console.WriteLine(string.Format("{0,-24} {1,-24}", item.TenSinhVien, item.SoMonHoc));
            }
            Console.WriteLine("======================");
            foreach (var item in tmp2)
            {
                Console.WriteLine(string.Format("{0,-24} {1,-24}", item.TenSinhVien, item.SoMonHoc));
            }
        }
        public void ThongKeDiemTrungBinhCuaSinhVien()
        {
            Console.WriteLine("\n\tDanh sach sinh vien va so mon hoc cua sinh vien do: ");
            var tmp = from sv in listSV
                      join gd in listGD on sv.MaSinhVien equals gd.MaSinhVien
                      into grByMaSV
                      select new
                      {
                          sv.HoTen,
                          SoLuong = grByMaSV.DefaultIfEmpty().Average(gd => gd?.Diem ?? 0)
                      };
            foreach (var item in tmp)
            {
                Console.WriteLine(item);

            }
        }

        public void InMonHocVaSoSinhVien()
        {
            Console.WriteLine("\nIn mon hoc va so luong sinh vien:");
            var list = from mh in listMH
                       join gd in listGD on mh.MaMonHoc equals gd.MaMonHoc into table
                       select new
                       {
                           mh.TenMonHoc,
                           SoLuongSinhVien = table.Count()
                       };
            Console.WriteLine(string.Format("{0,-30}{1,-12}", "Ten mon hoc", "So luong sinh vien"));
            foreach (var item in list)
            {
                Console.WriteLine(string.Format("{0,-30}{1,-12}", item.TenMonHoc, item.SoLuongSinhVien));
            }
        }
        public void InSoMonHocCoSinhVien()
        {
            var res = from gd in listGD
                      group gd by gd.MaMonHoc into GroupMaMH
                      select new
                      {
                          MaMH = GroupMaMH.Key,
                          SL = GroupMaMH.Count()
                      };
            Console.WriteLine("So luong mon hoc co sinh vien: " + res.Count());
        }
        public void InSoMonHocKhongCoSinhVien()
        {
            var res = from gd in listGD
                      group gd by gd.MaMonHoc into GroupMaMH
                      select new
                      {
                          MaMH = GroupMaMH.Key,
                          SL = GroupMaMH.Count()
                      };
            Console.WriteLine("So luong mon hoc khong co sinh vien: " + (listMH.Count - res.Count()));
        }

        public void Test()
        {
            var test = from s in listSV
                       join g in listGD on s.MaSinhVien equals g.MaSinhVien into G
                       select new
                       {
                           MaSV = s.MaSinhVien,
                           Num = G.Count()
                       };
            foreach (var g in test)
            {
                Console.WriteLine(g);
            }

            //var query = from s in listSV
            //            join g in listGD on s.MaSinhVien equals g.MaSinhVien into G
            //            from sub in G
            //            group sub by sub.MaSinhVien into G2
            //            select new
            //            {
            //                G2.Key,
            //                SL = G2.Count()
            //            };
            //foreach (var g in test)
            //{
            //    Console.WriteLine(g);
            //}
        }
    }
}
