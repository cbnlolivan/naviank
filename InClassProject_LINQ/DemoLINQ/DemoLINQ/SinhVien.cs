﻿using NPL.M.A010.Exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLINQ
{
    internal class SinhVien
    {
        public int MaSinhVien { get; set; }
        public string HoTen { get; set; }
        public int Tuoi { get; set; }
        public string Khoa { get; set; }

        public void NhapThongTin(bool cMa = true, bool cHo = true, bool cTuoi = true, bool cKhoa = true)
        {
            int maSV = 0;
            if (cMa)
            {
                this.MaSinhVien = NDF.InputSomethingOfType<int>("Ma sinh vien: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            }
            if (cHo)
            {
                this.HoTen = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Ten sinh vien: ", "Sai dinh dang roi, nhap lai di.", @"^[a-zA-Z ]+$"));
            }
            if (!cTuoi)
            {
                this.Tuoi = NDF.InputSomethingOfType<int>("Tuoi: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            }
            if (cKhoa)
            {
                this.Khoa = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Khoa: ", "Sai dinh dang roi, nhap lai di.", @"^[a-zA-Z0-9 ]+$"));
            }

        }
    }
}
