﻿using NPL.M.A010.Exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLINQ
{
    internal class MonHoc
    {
        public int MaMonHoc {  get; set; }
        public string TenMonHoc { get; set; }

        public MonHoc()
        {
        }

        public MonHoc(int maMonHoc, string tenMonHoc)
        {
            MaMonHoc = maMonHoc;
            TenMonHoc = tenMonHoc;
        }

        public void NhapThongTin(bool cMa = true, bool cTen = true)
        {
            if (cMa)
            {
                this.MaMonHoc = NDF.InputSomethingOfType<int>("Ma mon hoc: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            }
            if (cTen)
            {
                this.TenMonHoc = NDF.InputSomethingOfType<string>("Ten mon hoc: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            }
                /*
              
                          MonHoc res = new MonHoc();
            int maMH = NDF.InputSomethingOfType<int>("Ma mon hoc: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            if (ChonMonHocCoMaMH(maMH) != null)
            {
                Console.WriteLine("Ma mon hoc nay da ton tai roi nhe!");
                return null;
            }
            res.MaMonHoc = maMH;
            res.TenMonHoc = NDF.InputSomethingOfType<string>("Ten mon hoc: ", "Sai dinh dang roi, nhap lai di.", @"^\d+$");
            return res;*/
        }
        public override string? ToString()
        {
            return "Ma mon hoc: " + this.MaMonHoc + ", ten mon hoc: " + this.TenMonHoc;
        }
    }
}
