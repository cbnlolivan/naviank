﻿
using DemoLINQ;
using NPL.M.A010.Exercise;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        List<SinhVien> sinhViens = new List<SinhVien>
        {
            new SinhVien { MaSinhVien = 1, HoTen = "Phạm Khải Văn", Tuoi = 20, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 2, HoTen = "Trần Hồng Ngọc", Tuoi = 22, Khoa = "Toán học" },
            new SinhVien { MaSinhVien = 3, HoTen = "Lê Thùy Dung", Tuoi = 21, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 4, HoTen = "Nguyễn Thị Thu Trang", Tuoi = 23, Khoa = "Vật lý" },
            new SinhVien { MaSinhVien = 5, HoTen = "Đỗ Thùy Linh", Tuoi = 24, Khoa = "Toán học" }
        };
        List<MonHoc> monHocs = new List<MonHoc>
        {
            new MonHoc { MaMonHoc = 101, TenMonHoc = "Lập trình cơ bản" },
            new MonHoc { MaMonHoc = 102, TenMonHoc = "Toán cao cấp" },
            new MonHoc { MaMonHoc = 103, TenMonHoc = "Vật lý cơ bản" },
            new MonHoc { MaMonHoc = 104, TenMonHoc = "Triet hoc" }
        };
        List<GhiDanh> ghiDanhs = new List<GhiDanh>
        {
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 102 , Diem = 10},
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 101 , Diem = 9},
            new GhiDanh { MaSinhVien = 2, MaMonHoc = 101 , Diem = 8},
            new GhiDanh { MaSinhVien = 3, MaMonHoc = 103 , Diem = 8.5f},
            new GhiDanh { MaSinhVien = 4, MaMonHoc = 102 , Diem = 9.5f},
            new GhiDanh { MaSinhVien = 4, MaMonHoc = 103 , Diem = 7.5f}
        };
        /*
        Console.WriteLine("\tDanh sach mon hoc: ");
        var dsMonHoc = from m in monHocs
                       select m.TenMonHoc;
        foreach (var d in dsMonHoc)
        {
            Console.WriteLine(d);
        }

        Console.WriteLine();
        Console.WriteLine("\tDanh sach sinh vien: ");
        var dsSinhVien = from s in sinhViens
                         select new { s.MaSinhVien, s.HoTen };
        foreach (var item in dsSinhVien)
        {
            Console.WriteLine(item.MaSinhVien + ": " + item.HoTen);
        }

        Console.WriteLine();
        Console.WriteLine("\tDanh sach co dieu kien: ");
        var ds2 = from g in ghiDanhs
                  where g.MaMonHoc == 101
                  orderby g.MaSinhVien
                  select g;
        foreach (var d in ds2)
        {
            Console.WriteLine(d.MaSinhVien + ": 101");
        }

        Console.WriteLine();
        Console.WriteLine("\tResult with lambda expression: ");
        var listMonHoc = monHocs.Where(m => m.MaMonHoc == 101).Select(m => (m.MaMonHoc, m.TenMonHoc));
        foreach (var item in listMonHoc)
        {
            Console.WriteLine(item);
        }

        Console.WriteLine("\nMon hoc 1");
        List<MonHoc> monHoc1 = (List<MonHoc>)monHocs.Where(m => m.MaMonHoc == 104).Select(m => new MonHoc(m.MaMonHoc, m.TenMonHoc)).ToList();
        Console.WriteLine("Data type: " + monHoc1.GetType());

        MonHoc monHoc2 = new MonHoc();
        monHoc2.MaMonHoc = listMonHoc.FirstOrDefault().MaMonHoc;
        monHoc2.TenMonHoc = listMonHoc.FirstOrDefault().TenMonHoc;

        MonHoc monHoc3 = new MonHoc();
        monHoc3.MaMonHoc = listMonHoc.SingleOrDefault().MaMonHoc;
        monHoc3.TenMonHoc = listMonHoc.SingleOrDefault().TenMonHoc;

        Console.WriteLine("\nMon hoc 2: ");
        Console.WriteLine(monHoc2);
        Console.WriteLine("\nMon hoc 3: ");
        Console.WriteLine(monHoc3);

        Console.WriteLine("\nUsing join eperator:");
        var thongtinSinhVienQuery = from sv in sinhViens
                                    join gd in ghiDanhs on sv.MaSinhVien equals gd.MaSinhVien
                                    join mh in monHocs on gd.MaMonHoc equals mh.MaMonHoc
                                    where gd.MaMonHoc == 101
                                    orderby gd.MaMonHoc descending
                                    orderby sv.MaSinhVien descending
                                    select new
                                    {
                                        sv.HoTen,
                                        mh.TenMonHoc
                                    };
        var thongtinSinhVienQuery2 = from sv in sinhViens
                                     join gd in ghiDanhs on sv.MaSinhVien equals gd.MaSinhVien
                                     join mh in monHocs on gd.MaMonHoc equals mh.MaMonHoc
                                     orderby gd.MaMonHoc descending
                                     select (sv.HoTen, mh.TenMonHoc);
        Console.WriteLine(string.Format("{0, -30} {0, -30}", "TenMonHoc", "HoTen"));
        foreach (var item in thongtinSinhVienQuery)
        {
            Console.WriteLine(string.Format("{0, -30} {0, -30}", item.TenMonHoc, item.HoTen));
        }
        */

        QuanLy quanLy = new QuanLy(sinhViens, monHocs, ghiDanhs);
        /*
        int choice;
        do
        {
            Console.WriteLine("\tMenu:");
            Console.WriteLine("1. Thêm thông tin sinh viên");
            Console.WriteLine("2. Thêm thông tin môn học");
            Console.WriteLine("3. Đăng ký môn học cho sinh viên");
            Console.WriteLine("4. Thống kê danh sách sinh viên và số lượng môn học");
            Console.WriteLine("5. Thống kê môn học có bao nhiêu sinh viên");
            Console.WriteLine("6. Thống kê điểm trung bình của sinh viên");
            Console.WriteLine("7. Thống kê số lượng sinh viên");
            Console.WriteLine("8. Thống kê số lượng môn học có sinh viên");
            Console.WriteLine("9. Thống kê số lượng môn học không có sinh viên");
            Console.WriteLine("10. In danh sách sinh viên");
            Console.WriteLine("11. In danh sách môn học");
            Console.WriteLine("12. In danh sách đăng ký môn học");
            Console.WriteLine("0. Thoát");

            choice = NDF.GetUserChoiceAsIntNumber(0, 13, "Chọn chức năng: ");
            switch (choice)
            {
                case 1:
                    quanLy.NhapNhieuSinhVienMoi();
                    Console.WriteLine();
                    break;
                case 2:
                    quanLy.NhapNhieuMonHocMoi();
                    Console.WriteLine();
                    break;
                case 3:
                    quanLy.GhiDanh1SinhVien();
                    Console.WriteLine();
                    break;
                case 4:
                    quanLy.InDanhSachSinhVienVaSoMonHoc();
                    Console.WriteLine();
                    break;
                case 5:
                    quanLy.InMonHocVaSoSinhVien();
                    Console.WriteLine();
                    break;
                case 6:
                    quanLy.ThongKeDiemTrungBinhCuaSinhVien();
                    Console.WriteLine();
                    break;
                case 7:
                    quanLy.InSoLuongSinhVien();
                    Console.WriteLine();
                    break;
                case 8:
                    quanLy.InSoMonHocCoSinhVien();
                    Console.WriteLine();
                    break;
                case 9:
                    quanLy.InSoMonHocKhongCoSinhVien();
                    Console.WriteLine(); 
                    break;
                case 10:
                    quanLy.InDanhSachSinhVien();
                    Console.WriteLine(); 
                    break;
                case 11:
                    quanLy.InDanhSachMonHoc();
                    Console.WriteLine(); 
                    break;
                case 12:
                    quanLy.InDanhSachGhiDanh();
                    Console.WriteLine(); 
                    break;
                case 0:
                    Console.WriteLine("Exit program");
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Chức năng không hợp lệ.");
                    break;
            }
        } while (choice != 0);
        */
        quanLy.Test();
    }
}