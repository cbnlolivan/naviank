﻿/*
    Created by Naviank
 */

using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("=======Programm convert numbers from base 10 to base 2!=======");
        Boolean continiu = true;
        do
        {
            String DecimalValue = InputStringOfRegex(@"^[0-9]+$");
            //String Result = ConvertToBinary(DecimalValue);
            String Result = ConvertFromBase10ToBase(2, Int32.Parse(DecimalValue));
            Console.WriteLine("Result when convert " + DecimalValue + " from base 10 to base 2 is: " + Result);
            continiu = PressYNToContinue();
            Console.WriteLine();
        } while (continiu);
    }
    private static int PowerAnInteger(int basenumber, int exponent)
    {
        if (exponent == 0) { return 1; }
        int result = 1;
        for (int i = 1; i <= exponent; i++)
        {
            result *= basenumber;
        }
        return result;
    }
    private static String ConvertToBinary(String DecimalValue)
    {
        int tmp = 0;
        try
        {
            tmp = Convert.ToInt32(DecimalValue);
        }
        catch (Exception e)
        {
            Console.WriteLine("Can not parse the value to Decimal number!!!");
        }

        String Result = "";
        while (tmp > 0)
        {

            Result = tmp % 2 + "" + Result;
            tmp = tmp / 2;
        }
        if (Result.Equals("")) Result = "0";
        return Result;
    }

    private static int ConvertToBase10FromBase(int basenumber, String input)
    {
        int result = 0;
        int length = input.Length;
        for (int i = length - 1; i >= 0; i--)
        {
            result += Int32.Parse(input[i] + "") * PowerAnInteger(basenumber, length - i - 1);
        }
        return result;
    }
    
    private static String ConvertFromBase10ToBase(int basenumber, int input) 
    {
        String Result = "";
        if (input == 0) return 0 + "";
        while (input > 0)
        {

            Result = input % basenumber + "" + Result;
            input = input / basenumber;
        }
        return Result;
    }
    private static String InputStringOfRegex(String regex)
    {
        //Regex reg = new Regex(regex);
        while (true)
        {
            Console.Write("Enter a string of regex " + regex + ": ");
            String res = Console.ReadLine();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine("Accepted input: " + res);
                Console.WriteLine();
                return res;
            }
            else
            {
                Console.Write("Wrong format! Enter again: ");
            }
        }
    }

    private static Boolean PressYNToContinue()
    {
        Console.Write("Press Y to continue, Press N to stop the programm! ");
        Boolean res = "Y".Equals(Console.ReadLine().ToUpper());
        Console.WriteLine();
        return res;
    }
}