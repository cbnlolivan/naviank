﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("=======This programm check a number is prime or not!=======");
        Console.Write("Enter an integer: ");
        int Number = Int32.Parse(Console.ReadLine());
        Boolean res = CheckPrimeNumber(Number);
        Console.WriteLine(Number + (res ? " is" : " is not") + " a prime number!");
    }
    private static Boolean CheckPrimeNumber(int n)
    {
        for (int i = 2; i < Math.Sqrt(n); i++)
        {
            if (n % i == 0) return false;
        }
        return true;
    }
}