﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A001.Exercise1
{
    internal class QuadraticEquation
    {
        private double a { get; set; }
        private double b { get; set; }
        private double c { get; set; }
        public QuadraticEquation() { }
        public QuadraticEquation(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public void SolveEquation()
        {
            if (a == 0)
            {
                Console.WriteLine("This quadratic equation degenerates into a linear equation!");
                if (b == 0)
                {
                    if (c == 0) Console.WriteLine("This equation has infinite many roots!");
                    else Console.WriteLine("This equation has no roots");
                }
                else
                {
                    Console.WriteLine("This equation has only one real root x = " + (-c / b));
                }
            }
            else
            {
                double d = b * b - 4 * a * c;
                int nos = 0;
                double s1 = 0;
                double s2 = 0;
                if (d == 0)
                {
                    nos = 1;
                    s1 = -b / 2 * a;
                    s2 = -b / 2 * a;
                }
                else if (d < 0) { nos = 0; }
                else if (d > 0)
                {
                    nos = 2;
                    s1 = (-b + Math.Sqrt(d)) / (2 * a);
                    s2 = (-b - Math.Sqrt(d)) / (2 * a);
                }

                switch (nos)
                {
                    case 0:
                        Console.WriteLine("The equation has no real roots!!!");
                        break;
                    case 1:
                        Console.WriteLine("The equation has only one real root x = " + s1);
                        break;
                    case 2:
                        Console.WriteLine("The equation has 2 real roots: x1 = " + s1 + ", x2 = " + s2);
                        break;
                }
            }
        }
    }
}
