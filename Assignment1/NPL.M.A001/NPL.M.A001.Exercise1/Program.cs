﻿/*
    Created by Naviank
 */

using NPL.M.A001.Exercise1;
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        //int counter = 1;
        do
        {
            Console.WriteLine("Solve the quadratic equation: \"ax2+bx+c=0\"\n");
            double a = double.Parse(InputStringOfRegexWithMessage("Enter value of a: ", "Can not parse string to real number!!!", @"^[+-]?\d+(\.\d+)?$"));
            double b = double.Parse(InputStringOfRegexWithMessage("Enter value of b: ", "Can not parse string to real number!!!", @"^[+-]?\d+(\.\d+)?$"));
            double c = double.Parse(InputStringOfRegexWithMessage("Enter value of c: ", "Can not parse string to real number!!!", @"^[+-]?\d+(\.\d+)?$"));
            QuadraticEquation equation = new QuadraticEquation(a, b, c);
            Console.WriteLine("Result: ");
            equation.SolveEquation();
        } while (PressYNToContinue());
    }

    private static String InputStringOfRegexWithMessage(String message, String error, String regex)
    {
        while (true)
        {
            Console.Write(message);
            String res = Console.ReadLine();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine("Accepted input:" + res);
                Console.WriteLine();
                return res;
            }
            else
            {
                Console.Write(error);
            }
        }
    }

    private static Boolean PressYNToContinue()
    {
        Console.Write("\nPress Y to continue, Press N to stop the programm! ");
        Boolean res = "Y".Equals(Console.ReadLine().ToUpper());
        Console.WriteLine();
        return res;
    }
}