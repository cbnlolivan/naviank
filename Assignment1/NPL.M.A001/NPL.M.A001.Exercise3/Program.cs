﻿using System.Numerics;
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("=======This programm prints the first n fibonacci number!=======");
        List<BigInteger> FiboArr = new List<BigInteger>();
        do
        {
            int Number = 0;
            Number = Int32.Parse(InputStringOfRegex("Enter number of fibonacci number you want to print: ", @"^[0-9]+$"));
            Console.WriteLine();
            Console.WriteLine("Print " + Number + " first fibonacci number!");
            InitFibonacciArray(FiboArr, Number);
            PrintNFirstFibonacciNumber(FiboArr, Number);
        } while (PressYNToContinue());
    }

    private static void PrintNFirstFibonacciNumber(List<BigInteger> FiboArr, int n)
    {
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine(FiboArr[i]);
        }
    }

    private static List<BigInteger> InitFibonacciArray(List<BigInteger> FiboArr, int n)
    {
        if (n < FiboArr.Count) { }
        else
        {
            while (FiboArr.Count < n)
            {
                int length = FiboArr.Count;
                if (length == 0 || length == 1)
                {
                    FiboArr.Add(1);
                }
                else
                {
                    FiboArr.Add((FiboArr[length - 1]) + FiboArr[length - 2]);
                }
            }
        }
        return FiboArr;
    }

    private static String InputStringOfRegex(String message, String regex)
    {
        while (true)
        {
            Console.Write(message);
            String res = Console.ReadLine();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine("Accepted input: " + res);
                Console.WriteLine();
                return res;
            }
            else
            {
                Console.Write("Wrong format! Enter again: ");
            }
        }
    }

    private static Boolean PressYNToContinue()
    {
        Console.Write("Press Y to continue, Press N to stop the programm! ");
        Boolean res = "Y".Equals(Console.ReadLine().ToUpper());
        Console.WriteLine();
        return res;
    }

}