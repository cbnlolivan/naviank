﻿/*
    Created by Naviank
 */
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("This programm find out the max and min elements in an array!");
        int ArrayLength = 0;
        Console.Write("Enter the length of array before inputting: ");
        ArrayLength = Int32.Parse(Console.ReadLine());
        int[] array = new int[ArrayLength];
        int max = Int32.MinValue;
        int min = Int32.MaxValue;
        for (int i = 0; i < ArrayLength; i++)
        {
            Console.Write("Enter value of element at index = " + i + ": ");
            array[i] = Int32.Parse(Console.ReadLine());
            if (array[i] > max) max = array[i];
            if (array[i] < min) min = array[i];
        }

        Console.Write("Inputted array is:\n[");
        for (int i = 0;i < array.Length; i++)
        {
            if (i > 0)
            {
                Console.Write(", " + array[i]);
            } else
            {
                Console.Write(array[i]);
            }
        }
        Console.WriteLine("]");
        Console.WriteLine("Max value in this array is " + max);
        Console.WriteLine("Min value in this array is " + min);
    }
}