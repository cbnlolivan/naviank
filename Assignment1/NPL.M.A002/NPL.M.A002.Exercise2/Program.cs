﻿/*
    Created by Naviank
 */
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("=======This programm find the GCD of an integer array=======");
        Console.Write("Enter the first number: "); int n1 = Int32.Parse(Console.ReadLine());
        Console.Write("Enter the second number: "); int n2 = Int32.Parse(Console.ReadLine());
        Console.WriteLine("GCD of these numbers is: " + FindGCD(n1, n2));
        Console.ReadLine();
    }

    private static int FindGCD(int n1, int n2)
    {
        if (n1 == 0)
            return n2;
        return FindGCD(n2 % n1, n1);
    }

}