﻿/*
    Created by Naviank
 */
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("=======This programm find the GCD of an integer array=======");

        int ArrayLength = 0;
        Console.Write("Enter the length of array before inputting: ");
        ArrayLength = Int32.Parse(Console.ReadLine());
        int[] array = new int[ArrayLength];

        for (int i = 0; i < ArrayLength; i++)
        {
            Console.Write("Enter value of element at index = " + i + ": ");
            array[i] = Int32.Parse(Console.ReadLine());
        }

        Console.Write("Inputted array is:\n[");
        for (int i = 0; i < array.Length; i++)
        {
            if (i > 0)
            {
                Console.Write(", " + array[i]);
            }
            else
            {
                Console.Write(array[i]);
            }
        }
        Console.WriteLine("]");

        Console.WriteLine("GCD of this array is: " + FindGCDOfArray(array));
        Console.ReadLine();
    }

    private static int FindGCD(int n1, int n2)
    {
        if (n1 == 0)
            return n2;
        return FindGCD(n2 % n1, n1);
    }

    static int FindGCDOfArray(int[] arr)
    {
        int result = arr[0];
        foreach (int tmp in arr)
        {
            result = FindGCD(result, tmp);
            if (result == 1)
            {
                break;
            }
        }
        return result;
    }
}