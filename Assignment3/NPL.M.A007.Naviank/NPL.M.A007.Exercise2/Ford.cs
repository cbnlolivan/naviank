﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Ford : Car
    {
        private int Year {  get; set; }
        private int ManufacturerDiscount { get; set; }

        public Ford()
        {
        }

        public Ford(int year, int manufacturerDiscount)
        {
            Year = year;
            ManufacturerDiscount = manufacturerDiscount;
        }

        public Ford(decimal speed, double regularPrice, string color) : base(speed, regularPrice, color)
        {
        }

        public Ford(decimal speed, double regularPrice, string color, int year, int manufacturerDiscount) : base(speed, regularPrice, color)
        {
            this.Year = year;
            this.ManufacturerDiscount = manufacturerDiscount;
        }

        public override double GetSalePrice()
        {
            return base.GetSalePrice() - ManufacturerDiscount;
        }
        public override string GetCarType()
        {
            return "Ford";
        }
        public override string ToString()
        {
            return base.ToString() + String.Format("{0,8} {1, 8}", this.Year, this.ManufacturerDiscount);
        }
    }
}
