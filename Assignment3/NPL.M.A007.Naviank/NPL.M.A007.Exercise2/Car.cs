﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Car
    {
        private decimal Speed { get; set; }
        private double RegularPrice { get; set; }
        private String Color { get; set; }

        public Car()
        {
        }

        public Car(decimal speed, double regularPrice, string color)
        {
            Speed = speed;
            RegularPrice = regularPrice;
            Color = color;
        }

        public virtual double GetSalePrice()
        {
            return RegularPrice;
        }

        public virtual string GetCarType()
        {
            return "Car";
        }

        public override string? ToString()
        {
            String res = String.Format("{0, 8} {1, 16} {2, 8}", this.Speed, this.RegularPrice, this.Color);
            //return "Speed: " + Speed + ", RegularPrice: " + RegularPrice + ", Color: " + Color;
            return res;
        }
    }
}
