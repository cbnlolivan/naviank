﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("");
            List<Car> list = new List<Car>();
            Sedan sedan = new Sedan(100, 100000, "Red", 21);
            Ford ford1 = new Ford(812, 132000, "Pink", 2022, 1400);
            Ford ford2 = new Ford(297, 144000, "Green", 2022, 1200);
            Truck truck1 = new Truck(124, 169000, "Yellow", 1800);
            Truck truck2 = new Truck(80, 196000, "Blue", 7000);

            list.Add(sedan);
            list.Add(ford1);
            list.Add(ford2);
            list.Add(truck1);
            list.Add(truck2);
            Console.WriteLine(String.Format("{0, -12} {1, -20}", "Car type", "Sale price"));
            //Console.WriteLine(String.Format("{0, 8} {1, 16} {2, 8}", "Speed", "RegularPrice", "Color"));
            foreach (Car car in list)
            {
                Console.WriteLine(String.Format("{0, -12} {1, -20}", car.GetCarType(), car.GetSalePrice()));
                //Console.WriteLine(car);
            }
        }
    }
}
