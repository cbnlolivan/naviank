﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck : Car
    {
        private int Weight { get; set; }

        public Truck() { }
        public Truck(int Weight)
        {
            this.Weight = Weight;
        }

        public Truck(decimal speed, double regularPrice, string color) : base(speed, regularPrice, color)
        {
        }
        public Truck(decimal speed, double regularPrice, string color, int weight) : base(speed, regularPrice, color)
        {
            this.Weight = weight;
        }

        public override double GetSalePrice()
        {
            if (Weight > 2000) return 0.90 * base.GetSalePrice();
            else return 0.80 * base.GetSalePrice();
        }
        public override string GetCarType()
        {
            return "Truck";
        }

        public override string ToString()
        {
            string res = base.ToString() + String.Format("{0, 8}", this.Weight);
            return res;
        }
    }
}
