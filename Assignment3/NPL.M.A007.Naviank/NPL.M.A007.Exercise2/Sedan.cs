﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Sedan : Car
    {
        private int Length {  get; set; }

        public Sedan()
        {
        }

        public Sedan(decimal speed, double regularPrice, string color) : base(speed, regularPrice, color)
        {
        }        
        public Sedan(decimal speed, double regularPrice, string color, int Length) : base(speed, regularPrice, color)
        {
            this.Length = Length;
        }

        public override double GetSalePrice()
        {
            if (Length > 20) return 0.95 * base.GetSalePrice();
            else return 0.9 * base.GetSalePrice();
        }

        public override string GetCarType()
        {
            return "Sedan";
        }

        public override string ToString()
        {
            return base.ToString() + String.Format("{0,8}", this.Length);
        }
    }
}
