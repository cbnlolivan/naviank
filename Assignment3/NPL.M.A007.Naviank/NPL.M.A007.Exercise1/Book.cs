﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise1
{
    internal class Book
    {
        public String? Name { get; set; }
        public String? ISBN { get; set; }
        public String? Author { get; set; }
        public String? Publisher { get; set; }

        public Book()
        {
        }

        public Book(string? name, string? iSBN, string? author, string? publisher)
        {
            Name = name;
            ISBN = iSBN;
            Author = author;
            Publisher = publisher;
        }

        public String GetBookInformation()
        {
            return String.Format("{0, -12} {1, -20} {2, -20} {3, -20}", this.ISBN, this.Name, this.Author, this.Publisher);
        }
    }
}
