﻿using NPL.M.A007.Exercise1;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("\tThis programm gets books' information and prints out to console screen!\n");
        List<Book> books = new List<Book>();

        // Inserting 5 books
        books.Add(new Book("Book 1", "ISBN01", "Author 1", "Publisher 1"));
        books.Add(new Book("Book 2", "ISBN02", "Author 2", "Publisher 2"));
        books.Add(new Book("Book 3", "ISBN03", "Author 3", "Publisher 3"));
        books.Add(new Book("Book 4", "ISBN04", "Author 4", "Publisher 4"));
        books.Add(new Book("Book 5", "ISBN05", "Author 5", "Publisher 5"));

        String tmp = String.Format("{0, -12} {1, -20} {2, -20} {3, -20}", 
                                "ISBN Number", "Book Name", "Author Name", "Publisher Name");
        Console.WriteLine(tmp);
        foreach (Book book in books)
        {
            Console.WriteLine(book.GetBookInformation());
        }
    }
}