﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NPL.M.A010.Exercise
{
    internal class MailManagement
    {
        public List<OutlookMail> list { get; set; }
        public MailManagement()
        {
            list = new List<OutlookMail>();
        }

        //public OutlookMail InputNewMail(bool cFrom = true, bool cTo = true, bool cCc = true, bool cSubject = true,
        //    bool cAttach = true, bool cBody = true, bool cImportant = true, bool cPass = true)
        //{
        //    string from = "";
        //    if (cFrom)
        //        from = NDF.InputNormalEmail("From: ");

        //    List<string> to = new List<string>();
        //    List<string> cc = new List<string>();
        //    if (cTo)
        //    {
        //        int count = 0;
        //        do
        //        {
        //            to.Add(NDF.InputFsoftMail("To " + count++ + ": "));
        //        } while (NDF.PressYNToContinue("Press Y to continue: "));
        //    }
        //    if (cCc)
        //    {
        //        int count = 0;
        //        do
        //        {
        //            cc.Add(NDF.InputNormalEmail("To " + count++ + ": "));
        //        } while (NDF.PressYNToContinue("Press Y to continue: "));
        //    }
        //    string subject = "";
        //    if (cSubject)
        //    {
        //        subject = NDF.getNonEmptyString("Subject: ");
        //    }
        //    string attachmentPath = "";
        //    if (cAttach)
        //    {
        //        attachmentPath = NDF.getNonEmptyString("Attachment: ");
        //    }
        //    string mailBody = "";
        //    if (cBody)
        //    {
        //        mailBody = NDF.ReadLineOrCtrlEnter("Mail body:\n ");
        //    }

        //    bool isImportant = false;
        //    if (cImportant)
        //    {
        //        isImportant = NDF.getNonEmptyString("Is important: ").ToLower().Equals("yes");
        //    }
        //    string password = "";
        //    if (isImportant)
        //    {
        //        if (cPass)
        //        {
        //            password = NDF.EncryptPassword(NDF.getNonEmptyString("Password: "));
        //        }
        //    }
        //    OutlookMail outlookMail = new OutlookMail(from, to, cc, subject, attachmentPath, mailBody,
        //        isImportant, password, DateTime.Now, (OutlookMail.MailStatus)1);
        //    return outlookMail;
        //}

        // Menu Function 1: New Mail

        public void InsertNewMail(string filePath)
        {
            Console.WriteLine("===== Enter Email Information =====");
            OutlookMail outlookMail = new OutlookMail().InputOutlookMail();
            string tmpMenu = "====Mailing menu====";
            tmpMenu += "\n1. Send";
            tmpMenu += "\n2. Draft";
            tmpMenu += "\n3. Re-enter";
            tmpMenu += "\n4. Main menu";
            tmpMenu += "\nEnter option menu number: ";
            int choice = NDF.GetUserChoiceAsIntNumber(1, 4, tmpMenu);
            switch (choice)
            {
                case 1:
                    outlookMail.Status = (OutlookMail.MailStatus)1;
                    outlookMail.SentDate = DateTime.Now;
                    this.list.Add(outlookMail);
                    this.list.Sort();
                    Console.WriteLine("Email was added to Sent email!");
                    NDF.PrintListEmailToXmlFile(this.list, filePath);
                    break;
                case 2:
                    //outlookMail.SentDate = DateTime.Now;
                    outlookMail.Status = (OutlookMail.MailStatus)0;
                    this.list.Add(outlookMail);
                    this.list.Sort();
                    NDF.PrintListEmailToXmlFile(this.list, filePath);
                    Console.WriteLine("Email was added to Draft email!");
                    break;
                case 3:
                    Console.WriteLine("You required to re-enter the email!");
                    InsertNewMail(filePath);
                    break;
                case 4:
                    break;

            }
        }


        // Menu Function 2: Sent Emails
        public List<OutlookMail> GetSentEmails()
        {
            List<OutlookMail> sentList = new List<OutlookMail>();
            foreach (OutlookMail mail in this.list)
            {
                if (mail.Status == (OutlookMail.MailStatus)1) { sentList.Add(mail); }
            }
            return sentList;
        }
        // Menu Function 3: Draft Emails
        public List<OutlookMail> GetDraftEmails()
        {
            List<OutlookMail> draftList = new List<OutlookMail>();
            foreach (OutlookMail mail in this.list)
            {
                if (mail.Status == (OutlookMail.MailStatus)0) { draftList.Add(mail); }
            }
            return draftList;
        }
    }
}

/*
<OutlookEmail>
	<Mail>
		<From>From email 1</From>
		<To>
			<Address>To email 1</Address>
		</To>
		<Cc>
			<Address>Cc mail 1</Address>
			<Address>Cc mail 2</Address>
		</Cc>
		<Subject>Subject 1</Subject>
        <Attachment>Attachment path 1</Attachment>
        <Body>Body 1</Body>
        <IsImportant>Yes</IsImportant>
        <Password>encryptedpassword</Password>
        <Status>Sent</Status>
        <SentDate>2019/08/30 11:30 AM</SentDate>
    </Mail>
</OutlookEmail>
<OutlookEmail>
	<Mail>
		<From>From email 2</From>
		<To>
			<Address>To email 1</Address>
		</To>
		<Cc>
			<Address>Cc mail 1</Address>
			<Address>Cc mail 2</Address>
		</Cc>
		<Subject>Subject 2</Subject>
        <Attachment>Attachment path 3</Attachment>
        <Body>Body 2</Body>
        <IsImportant>Yes</IsImportant>
        <Password>encryptedpassword</Password>
        <Status>Sent</Status>
        <SentDate>2020/08/30 11:30 AM</SentDate>
    </Mail>
</OutlookEmail>
<OutlookEmail>
	<Mail>
		<From>From email 3</From>
		<To>
			<Address>To email 1</Address>
		</To>
		<Cc>
			<Address>Cc mail 1</Address>
			<Address>Cc mail 2</Address>
		</Cc>
		<Subject>Subject 3</Subject>
        <Attachment>Attachment path 3</Attachment>
        <Body>Body 3</Body>
        <IsImportant>Yes</IsImportant>
        <Password>encryptedpassword</Password>
        <Status>Sent</Status>
        <SentDate>2021/08/30 11:30 AM</SentDate>
    </Mail>
</OutlookEmail>
 */