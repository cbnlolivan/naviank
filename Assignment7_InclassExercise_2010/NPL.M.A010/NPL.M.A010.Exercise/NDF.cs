﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using static NPL.M.A010.Exercise.OutlookMail;

namespace NPL.M.A010.Exercise
{
    internal class NDF
    {
        // Input functions
        public static string InputstringOfRegexWithMessage(string message, string error, string regex)
        {
            while (true)
            {
                Console.Write(message);
                string res = Console.ReadLine().Trim();
                if (Regex.IsMatch(res, regex))
                {
                    Console.WriteLine("Accepted input:" + res);
                    Console.WriteLine();
                    return res;
                }
                else
                {
                    Console.WriteLine(error);
                }
            }
        }
        public static string InputNormalEmail(string mess)
        {
            string regex = @"^[A-Za-z]([^@]*)(?<![-.])([@]{1})(?![.-])[a-zA-Z0-9-.]+(?<![-.])$";
            string email = InputstringOfRegexWithMessage(mess, "Please enter email with format <account name>@<domain>", regex);
            return email;
        }
        public static string InputFsoftMail(string mess)
        {
            string regex = @"^([A-Za-z]([^@]*)(?<![-.])@fsoft.com.vn$)";
            string email = InputstringOfRegexWithMessage(mess, "Please enter email with format <account name>@fsoft.com.vn", regex);
            return email;
        }
        public static List<string> InputMultipleFsoftMail(string mess)
        {
            List<string> list = new List<string>();
            string regex = @"^([A-Za-z][^@]*(?<![-.])@fsoft.com.vn(?:\s*,\s*)?)+$";
            string email = InputstringOfRegexWithMessage(mess, "Please enter email with format <account name>@fsoft.com.vn", regex);
            string[] arr = Regex.Split(email, "\\s*,\\s*");
            foreach (string item in arr)
            {
                if (!list.Contains(item))
                    list.Add(item);
            }
            return list;
        }
        public static string InputPhoneNumber(int minLength, string mess)
        {
            string regex = @"^[0-9]{" + minLength + @",}$";
            string phoneNum = InputstringOfRegexWithMessage(mess, "Invalid phone number!!", regex).Replace("\\s+", "");
            return phoneNum;
        }
        public static Boolean PressYNToContinue(string mess)
        {
            Boolean res = "Y".Equals(InputstringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
            return res;
        }
        public static int GetUserChoiceAsIntNumber(int lowerbound, int upperbound, string mess)
        {
            int res;
            while (true)
            {
                //Console.Write(mess);
                res = Convert.ToInt32(InputstringOfRegexWithMessage(mess, "Only digit characters accepted!", @"^\d+$"));
                if (res >= lowerbound && res <= upperbound)
                    return res;
                else
                    Console.WriteLine("Invalid choice! You'll have to input again!");
            }
        }
        public static string getNonEmptyString(string mess)
        {
            string ret = "";
            while (true)
            {
                Console.Write(mess);
                ret = Regex.Replace(Console.ReadLine(), "\\s+", " ");
                if (ret.Equals(""))
                {
                    Console.WriteLine("Please input Non-Empty string!!!");
                    continue;
                }
                return ret;
            }
        }
        public static string ReadLineOrCtrlEnter(string mess)
        {
            string retstring = "";
            string curLine = "";
            int curIndex = 0;
            Console.WriteLine(mess);
            do
            {
                ConsoleKeyInfo readKeyResult = Console.ReadKey(true);
                // handle Ctrl + Enter
                if (readKeyResult.Key == ConsoleKey.Enter && (readKeyResult.Modifiers & ConsoleModifiers.Control) != 0)
                {
                    if (!(curIndex == 0))
                    {
                        retstring += curLine;
                        Console.WriteLine();
                    }
                    return retstring;
                }
                // handle Enter
                if (readKeyResult.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    if (curIndex != 0)
                    {
                        retstring += curLine;
                        retstring += "\n";
                        curLine = "";
                    }
                    curIndex = 0;
                }
                // handle backspace
                else if (readKeyResult.Key == ConsoleKey.Backspace)
                {
                    if (curIndex > 0)
                    {
                        curLine = curLine.Remove(curLine.Length - 1);
                        Console.Write(readKeyResult.KeyChar);
                        Console.Write(' ');
                        Console.Write(readKeyResult.KeyChar);
                        curIndex--;
                    }
                }
                else
                // handle all other keypresses
                {
                    curLine += readKeyResult.KeyChar;
                    Console.Write(readKeyResult.KeyChar);
                    curIndex++;
                }
            }
            while (true);
        }
        public static List<string> InputMultipleLine(string mess)
        {
            Console.WriteLine(mess);
            List<string> res = new List<string>();
            string newLine = "";
            bool check = true;
            while (true)
            {
                newLine = Console.ReadLine();
                if (newLine != null && !newLine.Equals(""))
                {
                    if (!newLine.ToUpper().Contains("<END>"))
                    {
                        res.Add(newLine);
                    }
                    else
                    {
                        Match m = Regex.Match(newLine, "<END>");
                        string tmp = newLine.Substring(0, m.Index);
                        res.Add($"{tmp}");
                        break;
                    }
                }
            }

            return res;
        }
        // Data handling / manipulating functions
        public static List<string> DevideParagraphToList(string input)
        {
            List<string> list = new List<string>();
            string[] tmp = Regex.Split(input, @"\n+");
            foreach (string s in tmp)
            {
                list.Add(s);
            }
            return list;
        }
        public static string EncryptPassword(string password)
        {
            return password;
        }
        public static string NormalizeName(string name)
        {
            string res = "";
            string regex = "\\s+";
            string[] strings = name.Trim().Split(regex);
            string[] nameArr = strings;
            for (int i = 0; i < nameArr.Length; i++)
            {
                nameArr[i] = nameArr[i].ToLower();
                res += nameArr[i].Substring(0, 1) + nameArr[i].Substring(1);
                if (i < nameArr.Length) res += " ";
            }
            return res;
        }
        public static OutlookMail ReadEmailFromXmlNode(XmlElement mail)
        {
            OutlookMail res = new OutlookMail();
            string from = mail.SelectSingleNode("From").InnerText;
            // Read <To> element
            XmlNodeList toNodeList = mail.SelectNodes("To/Address");
            List<string> toList = new List<string>();
            foreach (XmlNode to in toNodeList)
            {
                string tmp = to.InnerText;
                toList.Add(tmp);
            }
            // Read <Cc> element
            XmlNodeList? ccNodeList = mail.SelectNodes("Cc/Address");
            List<string> ccList = new List<string>();
            foreach (XmlNode ccNode in ccNodeList)
            {
                string tmp = ccNode.InnerText;
                ccList.Add(tmp);
            }
            string subject = mail.SelectSingleNode("Subject").InnerText;
            string attach = mail.SelectSingleNode("Attachment").InnerText;
            string bodyraw = mail.SelectSingleNode("Body").InnerText;
            List<string> body = DevideParagraphToList(bodyraw);
            bool isImportant = mail.SelectSingleNode("IsImportant").InnerText.Equals("Yes");
            string password = mail.SelectSingleNode("Password").InnerText;
            string status_raw = mail.SelectSingleNode("Status").InnerText;
            MailStatus status = (MailStatus)(status_raw == "Sent" ? 1 : 0);
            DateTime sentDate = DateTime.Parse(mail.SelectSingleNode("SentDate").InnerText);
            res = new OutlookMail(from, toList, ccList, subject, attach, body, isImportant, password, sentDate, status);
            return res;
        }
        public static List<OutlookMail> LoadMailFromXmlFile(string fileName)
        {
            List<OutlookMail> result = new List<OutlookMail>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
            XmlNodeList? mailNodes = xmlDoc.SelectNodes("/OutlookEmail/Mail");
            if (mailNodes == null)
            {
                Console.WriteLine("Cannot load data from an empty file/file path !!!");
            }
            foreach (XmlElement mail in mailNodes)
            {
                OutlookMail outlookMail = ReadEmailFromXmlNode(mail);
                result.Add(outlookMail);
            }
            return result;
        }
        public static XmlElement ConvertMailToXmlElement(OutlookMail mail, XmlDocument xmlDoc)
        {
            //XmlDocument xmlDoc = new XmlDocument();
            XmlElement mailElement = xmlDoc.CreateElement("Mail");
            // Create child elements of <Mail> element
            XmlElement from = xmlDoc.CreateElement("From");
            XmlElement to = xmlDoc.CreateElement("To");
            XmlElement cc = xmlDoc.CreateElement("Cc");
            XmlElement subject = xmlDoc.CreateElement("Subject");
            XmlElement attach = xmlDoc.CreateElement("Attachment");
            XmlElement body = xmlDoc.CreateElement("Body");
            XmlElement isImportant = xmlDoc.CreateElement("IsImportant");
            XmlElement password = xmlDoc.CreateElement("Password");
            XmlElement sentDate = xmlDoc.CreateElement("SentDate");
            XmlElement status = xmlDoc.CreateElement("Status");
            // Set values for child elements
            from.InnerText = mail.From.ToString();
            foreach (string tmp in mail.To)
            {
                XmlElement addressElement = xmlDoc.CreateElement("Address");
                addressElement.InnerText = tmp;
                to.AppendChild(addressElement);
            }
            foreach (string tmp in mail.Cc)
            {
                XmlElement addressElement = xmlDoc.CreateElement("Address");
                addressElement.InnerText = tmp;
                cc.AppendChild(addressElement);
            }
            subject.InnerText = mail.Subject.ToString();
            attach.InnerText = mail.AttachmentPath.ToString();
            //body.InnerText = "\n" + mail.MailBody.ToString();
            body.InnerText += "\n";
            foreach (string tmp in mail.MailBody)
            {
                body.InnerText += tmp + "\n";
            }
            body.InnerText += "\t";
            isImportant.InnerText = mail.GetIsImportantAsString();
            password.InnerText = mail.EncryptedPassword.ToString();
            sentDate.InnerText = mail.SentDate.ToString();
            status.InnerText = mail.Status.ToString();
            // Append child to <Mail> element
            mailElement.AppendChild(from);
            mailElement.AppendChild(to);
            mailElement.AppendChild(cc);
            mailElement.AppendChild(subject);
            mailElement.AppendChild(attach);
            mailElement.AppendChild(body);
            mailElement.AppendChild(isImportant);
            mailElement.AppendChild(password);
            mailElement.AppendChild(sentDate);
            mailElement.AppendChild(status);

            return mailElement;
        }
        public static void PrintListEmailToXmlFile(List<OutlookMail> listMail, string fileName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement root = xmlDoc.CreateElement("OutlookEmail");
            xmlDoc.AppendChild(root);

            foreach (OutlookMail mail in listMail)
            {
                XmlElement mailElement = ConvertMailToXmlElement(mail, xmlDoc);
                root.AppendChild(mailElement);
            }
            xmlDoc.Save(fileName);
            Console.WriteLine("Email list was updated at filepath: " + fileName);
        }

        // Print out results in the specified format
        public static void PrintListEmail(List<OutlookMail> list, string mess)
        {
            Console.WriteLine(mess);
            Console.WriteLine();
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine("Email number " + (i + 1) + ": " + list[i]);
            }
        }

        //public static void Main(string[] args)
        //{
        //    do
        //    {
        //        List<string> res = NDF.InputMultipleLine("Input: ");
        //        Console.WriteLine("res: ");
        //        foreach (string s in res)
        //            Console.WriteLine("" + s);
        //    } while (PressYNToContinue("Continue: "));
        //}
    }

}
