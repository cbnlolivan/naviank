﻿using NPL.M.A010.Exercise;
using System.Xml;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("====== Assignment 11 - Outlook Emulator ======");
        MailManagement manager = new MailManagement();
        string filePath = "D:\\Workspace\\OJT\\Tech\\NPL\\KS05.NetTraining\\naviank\\Assignment7_InclassExercise_2010\\NPL.M.A010\\OutlookMail.xml";

        string initString = "\nWorking file path: " + filePath;
        initString += "\n\nInit the Email list before running program: ";
        initString += "\n1. Working with new empty file";
        initString += "\n2. Working with old file (If it existed)";
        initString += "\n0. Exit program!";
        initString += "\nSelect an option by pressing the corresponding integer: ";
        bool check = true;
        do
        {
            switch (NDF.GetUserChoiceAsIntNumber(1, 2, initString))
            {
                case 1:
                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        // Load the XML file
                        xmlDoc.Load(filePath);
                        // Clear the content of the XML document
                        xmlDoc.DocumentElement.RemoveAll();
                        // Save the XML document to the same file
                        xmlDoc.Save(filePath);
                        check = false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Path not find!");
                    }
                    break;
                case 2:
                    manager.list = NDF.LoadMailFromXmlFile(filePath);
                    check = false;
                    break;
                case 0:
                    Console.WriteLine("Exit program with no changes found!");
                    break;
            }
        } while (check);

        string menu = "Please select the admin area you require: ";
        menu += "\n1. New mail.";
        menu += "\n2. Sent.";
        menu += "\n3. Draft.";
        menu += "\n4. Exit.";
        int userChoice;
        do
        {
            Console.WriteLine(menu);
            userChoice = NDF.GetUserChoiceAsIntNumber(1, 4, "Enter menu option number: ");
            switch (userChoice)
            {
                case 1:
                    manager.InsertNewMail(filePath);
                    break;
                case 2:
                    NDF.PrintListEmail(manager.GetSentEmails(), "List of sent emails:");
                    break;
                case 3:
                    NDF.PrintListEmail(manager.GetDraftEmails(), "List of drafted emails:");
                    break;
                case 4:
                    Console.WriteLine("Exit programm!");
                    break;

            }
        } while (userChoice != 4);
    }
}