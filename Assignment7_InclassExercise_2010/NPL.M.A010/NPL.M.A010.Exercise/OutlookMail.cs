﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A010.Exercise
{
    internal class OutlookMail : IComparable<OutlookMail>
    {
        public string From { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public string Subject { get; set; }
        public string AttachmentPath { get; set; }
        public List<string> MailBody { get; set; }
        public bool IsImportant { get; set; }
        public string EncryptedPassword { get; set; }
        public DateTime SentDate { get; set; }
        public MailStatus Status { get; set; }
        public enum MailStatus
        {
            Draft,
            Sent
        }
        public OutlookMail() { 
            From = string.Empty;
            Cc = new List<string>();
            To = new List<string>();
            Subject = string.Empty;
            AttachmentPath = string.Empty;
            MailBody = new List<string>();
            EncryptedPassword = string.Empty;
        }

        public OutlookMail(string from, List<string> to, List<string> cc, string subject, string attachmentPath,
            List<string> mailBody, bool isImportant, string encryptedPassword, DateTime sentDate, MailStatus status)
        {
            this.From = from;
            this.To = to;
            this.Cc = cc;
            this.Subject = subject;
            this.AttachmentPath = attachmentPath;
            this.MailBody = mailBody;
            this.IsImportant = isImportant;
            this.EncryptedPassword = encryptedPassword;
            this.SentDate = sentDate;
            this.Status = status;
        }

        public string GetIsImportantAsString()
        {
            return this.IsImportant ? "Yes" : "No";
        }

        public OutlookMail InputOutlookMail(bool cFrom = true, bool cTo = true, bool cCc = true, bool cSubject = true,
            bool cAttach = true, bool cBody = true, bool cImportant = true, bool cPass = true)
        {
            if (cFrom)
            {
                this.From = NDF.InputNormalEmail("From: ");
            }

            if (cTo)
            {
                To = NDF.InputMultipleFsoftMail("To (emails seperated by ','):");
            }
            if (cCc)
            {
                Cc = NDF.InputMultipleFsoftMail("Cc (emails seperated by ','):");
            }

            if (cSubject)
                this.Subject = NDF.getNonEmptyString("Subject: ");
            if (cAttach)
                this.AttachmentPath = NDF.getNonEmptyString("Attachment: ");
            if (cBody)
                //this.MailBody = NDF.ReadLineOrCtrlEnter("Mail body: ").Trim() + "\n\t";
                this.MailBody = NDF.InputMultipleLine("Mail body (type <END> to stop. Any characters after <END> will not be accepted!):");

            if (cImportant)
                this.IsImportant = NDF.getNonEmptyString("Is important: ").ToLower().Equals("yes");
            if (this.IsImportant)
            {
                this.EncryptedPassword = NDF.EncryptPassword(NDF.getNonEmptyString("Password: "));
            }
            Console.WriteLine();
            return this;
        }

        public override string? ToString()
        {
            string res = "";
            res += "\nFrom: " + this.From;

            res += String.Format("\n{0,-10}", "To:");
            foreach (var element in this.To)
            {
                res += String.Format("\n{0,-4} {1,-10}", " ", element);
            }
            res += String.Format("\n{0,-10}", "Cc:");
            foreach (var element in this.Cc)
            {
                res += String.Format("\n{0,-4} {1,-10}", " ", element);
            }

            res += "\nSubject: " + this.Subject;
            res += "\nAttachment: " + this.AttachmentPath;
            res += "\nMail body: ";
            foreach (string s in this.MailBody)
            {
                res += s + "\n";
            }
            res += "\nIs important: " + (this.IsImportant ? "Yes" : "No");
            if (this.IsImportant)
                res += "\nPassword: " + this.EncryptedPassword;
            res += "\nStatus: " + (this.Status == (MailStatus)1 ? "Sent" : "Drafted");
            res += "\nLast modified on: " + this.SentDate;
            res += "\n";
            return res;
        }
        public int CompareTo(OutlookMail? other)
        {
            int res = this.Subject.CompareTo(other.Subject);
            if (res == 0)
            {
                res = this.SentDate.CompareTo(other.SentDate);
            }
            return res;
        }
    }
}
