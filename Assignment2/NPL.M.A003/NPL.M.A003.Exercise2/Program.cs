﻿/*
    Created by Naviank
 */
using System.Diagnostics.Metrics;
using System.Text;
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;

        //string input = InputMultipleLines("Nhập văn bản. Nhấn Ctrl + Enter để kết thúc:").Trim();
        string input = ReadLineOrCtrlEnter("Nhập văn bản. Nhấn Ctrl + Enter để kết thúc:").Trim();
        Console.WriteLine("\nAfter sorting");

        string[] lineArr = Regex.Split(input, @"\n");
        string[] res = new string[lineArr.Length];
        res[0] = lineArr[0];

        for (int i = 1; i < res.Length; i++)
        {
            DateTime newValue = GetDateTimeInString(lineArr[i]);
            int counter = i - 1;
            while (counter >= 0 && newValue.CompareTo(GetDateTimeInString(res[counter])) < 0)
            {
                res[counter + 1] = res[counter];
                counter--;
            }
            res[counter + 1] = lineArr[i];
        }
        foreach (string tmp in res)
        {
            Console.WriteLine(tmp);
        }
    }

    public static DateTime GetDateTimeInString(string input)
    {
        DateTime res = DateTime.Now.AddDays(1);
        if (!string.IsNullOrWhiteSpace(input))
        {
            string matchvalue = Regex.Match(input, @"(\d{2}\/\d{2}\/\d{4})( (\d{2}:\d{2}:\d{2} (AM|PM)))?").Value;
            if (matchvalue.Equals("")) return res;
            if (matchvalue.Length > 10)
            {
                res = DateTime.ParseExact(matchvalue, "dd/MM/yyyy hh:mm:ss tt", null);
            }
            else
            {
                res = DateTime.ParseExact(matchvalue, "dd/MM/yyyy", null);
            }
        }
        return res;
    }
    public static String InputMultipleLines(String mess)
    {
        Console.WriteLine(mess);
        string input = string.Empty;
        int counter = 0;
        while (true)
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey(intercept: true);
            if (keyInfo.Key == ConsoleKey.Enter && keyInfo.Modifiers == ConsoleModifiers.Control)
                break;

            if (keyInfo.Key == ConsoleKey.Enter)
            {
                if (counter == 0)
                {
                    input += Environment.NewLine;
                    counter++;
                }
                Console.WriteLine();
            }
            else
            {
                counter = 0;
                input += keyInfo.KeyChar;
                Console.Write(keyInfo.KeyChar);
            }
        }
        return input;
    }

    public static String ReadLineOrCtrlEnter(String mess)
    {
        string retString = "";
        string curLine = "";
        int curIndex = 0;
        Console.WriteLine(mess);
        do
        {
            ConsoleKeyInfo readKeyResult = Console.ReadKey(true);
            // handle Ctrl + Enter
            if (readKeyResult.Key == ConsoleKey.Enter && (readKeyResult.Modifiers & ConsoleModifiers.Control) != 0)
            {
                Console.WriteLine();
                return retString;
            }
            // handle Enter
            if (readKeyResult.Key == ConsoleKey.Enter)
            {
                Console.WriteLine();
                if (curIndex != 1)
                {
                    if (GetDateTimeInString(curLine) != DateTime.Now.AddDays(1))
                        retString += curLine;
                    retString += "\n";
                    curLine = "";
                }
                curIndex = 0;
            }
            // handle backspace
            if (readKeyResult.Key == ConsoleKey.Backspace)
            {
                if (curIndex > 0)
                {
                    retString = retString.Remove(retString.Length - 1);
                    Console.Write(readKeyResult.KeyChar);
                    Console.Write(' ');
                    Console.Write(readKeyResult.KeyChar);
                    curIndex--;
                }
            }
            else
            // handle all other keypresses
            {
                curLine += readKeyResult.KeyChar;
                Console.Write(readKeyResult.KeyChar);
                curIndex++;
            }
            //Console.WriteLine(curIndex);
        }
        while (true);
    }
}