﻿/*
    Created by Naviank
 */
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("==== This programm calculate the datetime values ====");
        do
        {
            Console.Write("Enter date (follow the format dd/mm/yyyy): ");
            String TimeRaw = Console.ReadLine();
            String[] TimeArr = TimeRaw.Split("/");
            int Year = 2023;
            int Month = 1;
            int Date = 1;
            Date = Int32.Parse(TimeArr[0]);
            Month = Int32.Parse(TimeArr[1]);
            Year = Int32.Parse(TimeArr[2]);
            DateTime Time = new DateTime(Year, Month, Date);
            Console.WriteLine(Time);

            Console.WriteLine("Result");
            Time = AddWorkingDate(Time, 7);
            Console.WriteLine("1st reminder: " + Time.ToString("dd//MM/yyyy"));
            Time = AddWorkingDate(Time, 2);
            Console.WriteLine("2nd reminder: " + Time.ToString("dd//MM/yyyy"));
            Time = AddWorkingDate(Time, 1);
            Console.WriteLine("3rd reminder: " + Time.ToString("dd//MM/yyyy"));
            Time = AddWorkingDate(Time, 1);
            Console.WriteLine("4th reminder: " + Time.ToString("dd//MM/yyyy"));
            Time = AddWorkingDate(Time, 1);
            Console.WriteLine("5th reminder: " + Time.ToString("dd//MM/yyyy"));
        } while (SelectYorNToContinue());
    }

    public static DateTime AddWorkingDate(DateTime dateTime, int numofday)
    {
        while (numofday > 0)
        {
            int tmp = (int)dateTime.DayOfWeek;
            if (tmp != 5 && tmp != 6) numofday--;
            dateTime = dateTime.AddDays(1);
        }
        return dateTime;
    }

    public static Boolean SelectYorNToContinue()
    {
        Console.Write("Press Y to continue/N to stop: ");
        String tmp = Console.ReadLine();
        return "Y".Equals(tmp.ToUpper());
    }

}