﻿/*
   Created by Naviank
*/

using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("This programm validate the input string is an email or not!\n");
        do
        {
            Console.Write("Enter an email to validate: ");
            String input = Console.ReadLine();
            Boolean result = IsEmail(input);
            if (result == true)
            {
                Console.WriteLine("Accepted email!!!");
                break;
            }
            else
            {
                Console.WriteLine("Wrong email format!!!\n");
            }
        } while (true);
    }

    private static Boolean IsEmail(string email)
    {
        String EmailRegex = @"^[A-Za-z]([^@]*)(?<![-.])([@]{1})(?![.-])[a-zA-Z0-9-.]+(?<![-.])$";
        return Regex.IsMatch(email, EmailRegex);
    }
}