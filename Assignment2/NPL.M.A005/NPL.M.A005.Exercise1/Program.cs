﻿/*
   Created by Naviank
*/
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("This programm normalize a name!");
        Console.WriteLine();

        String name = InputStringOfRegex(@"^[a-zA-Z ]+$");
        Console.WriteLine();
        Console.WriteLine(NormalizeName(name));
    }

    private static String NormalizeName(string name)
    {
        String res = "";
        string[] strings = Regex.Split(name.Trim(), "\\s+");
        string[] nameArr = strings;
        for (int i = 0; i < nameArr.Length; i++)
        {
            nameArr[i] = nameArr[i].Trim().ToLower();
            res += nameArr[i].Substring(0, 1).ToUpper() + nameArr[i].Substring(1);
            if (i < nameArr.Length) res += " ";
        }
        return res;
    }

    private static String InputStringOfRegex(String regex)
    {
        while (true)
        {
            Console.Write("Enter a string of regex " + regex + ": ");
            String res = Console.ReadLine();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine("Accepted input: " + res);
                return res;
            }
            else
            {
                Console.Write("Wrong format! Enter again: ");
            }
        }
    }

    public static String RemoveUnnecessaryBlank(String input)
    {
        return input.Trim().Replace("//s+", " ");
    }

    public static String RemoveAllBlank(String input)
    {
        return input.Trim().Replace("//s+", "");
    }

}