﻿/*
   Created by Naviank
*/
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("This programm allows users to input list of string as name then sort these names by alphabetical order");
        String regex = @"^[a-zA-Z, ]+$";
        String input = InputStringOfRegexWithMessage("Enter list of name (seperated by ','):\n", regex);
        String[] NameArr = SortName(Regex.Split(input, ", "));
        Console.WriteLine();
        PrintStringArr(NameArr);
    }

    private static String[] GetLastNameArray(String[] NameArr)
    {
        String[] Result = new String[NameArr.Length];
        for (int i = 0; i < NameArr.Length; i++)
        {
            String[] tmp = Regex.Split(NameArr[i].Trim(), "\\s+");
            if (tmp.Length > 0)
                Result[i] = tmp[tmp.Length - 1];
        }
        return Result;
    }

    private static String[] SortName(String[] NameArr)
    {
        String[] LastNameArr = GetLastNameArray(NameArr);
        int n = LastNameArr.Length;
        for (int i = 0; i < n - 1; i++)
        {
            for (int j = 0; j < n - i - 1; j++)
            {
                if (String.Compare(LastNameArr[j], LastNameArr[j + 1]) > 0)
                {
                    String tmp = LastNameArr[j];
                    LastNameArr[j] = LastNameArr[j + 1];
                    LastNameArr[j + 1] = tmp;

                    String tmp2 = NameArr[j];
                    NameArr[j] = NameArr[j + 1];
                    NameArr[j + 1] = tmp2;
                }
            }
        }
        return NameArr;
    }
    private static String InputStringOfRegexWithMessage(String message, String regex)
    {
        while (true)
        {
            Console.Write(message);
            String res = Console.ReadLine();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine("\nAccepted input:\n" + res);
                return res;
            }
            else
            {
                Console.Write("Wrong format! Enter again: ");
            }
        }
    }
    private static void PrintStringArr(String[] strArr)
    {
        Console.WriteLine("String after sorting:");
        for (int i = 0; i < strArr.Length; i++)
        {
            Console.Write(strArr[i]);
            if (i < strArr.Length - 1)
                Console.Write(", ");
        }
        Console.ReadLine();
    }
}