﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class SalariedEmployee : Employee
    {
        private double CommissionRate { get; set; }
        private double GrossSales { get; set; }
        private double BasicSalary { get; set; }

        public SalariedEmployee() { }

        public SalariedEmployee(double commissionRate, double grossSales, double basicSalary)
        {
            CommissionRate = commissionRate;
            GrossSales = grossSales;
            BasicSalary = basicSalary;
        }
        public SalariedEmployee(String SSN, String FirstName, String LastName, DateTime BirthDate, String Phone, String Email, double CommissionRate, double GrossSales, double BasicSalary)
        {
            this.SSN = SSN;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.BirthDate = BirthDate;
            this.Phone = Phone;
            this.Email = Email;
            this.CommissionRate = CommissionRate;
            this.GrossSales = GrossSales;
            this.BasicSalary = BasicSalary;
        }
        public SalariedEmployee(string SSN, string FirstName, string LastName) : base(SSN, FirstName, LastName)
        {
        }
        public SalariedEmployee(string SSN, string FirstName, string LastName, DateTime BirthDate, string Phone, string Email) : base(SSN, FirstName, LastName, BirthDate, Phone, Email)
        {
        }
        public override string ToString()
        {
            return base.ToString() + String.Format("{0, 12} {1, 12} {2, 12}", CommissionRate, GrossSales, BasicSalary);
        }

    }
}
