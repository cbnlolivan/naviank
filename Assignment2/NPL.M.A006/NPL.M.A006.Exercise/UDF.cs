﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class UDF
    {
        /// <summary>
        /// Allow user input a line from keyboard follow the printed message. 
        /// Programm will validate whether the input string match the regex pattern. 
        /// If not match, print the error message and force user to input again. 
        /// Finally, return the string as ouput.
        /// </summary>
        /// <param name="message">String message which is printed before accepting input from user</param>
        /// <param name="error">String message which is printed when inputted string is not match the regex pattern</param>
        /// <param name="regex">String pattern that the output string must match</param>
        /// <returns>Return a string that match the given regex pattern</returns>
        public static String InputStringOfRegexWithMessage(String message, String error, String regex)
        {
            while (true)
            {
                Console.Write(message);
                String res = Console.ReadLine();
                if (Regex.IsMatch(res, regex))
                {
                    Console.WriteLine("Accepted input:" + res);
                    Console.WriteLine();
                    return res;
                }
                else
                {
                    Console.Write(error);
                }
            }
        }
        /// <summary>
        /// Call the InputStringOfRegexWithMessage function to get a string of birthdate. 
        /// As the business requirements, the string input must follow the format dd/MM/yyyy.
        /// </summary>
        /// <param name="mess"></param>
        /// <returns></returns>
        public static DateTime InputBirthDate(String mess)
        {
            String TimeRaw = InputStringOfRegexWithMessage("Enter date (follow the format dd/mm/yyyy): ", "Wrong date format!!!", @"^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$");
            String[] TimeArr = TimeRaw.Split("/");
            int Date = Int32.Parse(TimeArr[0]);
            int Month = Int32.Parse(TimeArr[1]);
            int Year = Int32.Parse(TimeArr[2]);
            DateTime result = new DateTime(Year, Month, Date);
            return result;
        }
        /// <summary>
        /// Call the InputStringOfRegexWithMessage function to get a string of Email. 
        /// As the business requirements, the string input must satisfiy the following conditions:
        /// 1. The local-part must start by an alphabetical character, then follow by any characters except @, and end not by . or - character
        /// 2. @ character locate between 2 part of an email.
        /// 3. The domain part must not start by - or . character, and contain only 0-9, a-z, A-Z characters.
        /// </summary>
        /// <param name="mess"></param>
        /// <returns></returns>
        public static String InputEmail(String mess)
        {
            //String domainRegex = @"^@(?![.-])[a-zA-Z0-9-.]+(?<![-.])$";
            //String localRegex = @"^(?![-.])[\w!#$%&'*+\/=?^`{|}~]+(?:\.[\w!#$%&'*+\/=?^`{|}~-]+)*(?<![-.])@$";

            String regex = @"^[A-Za-z]([^@]*)(?<![-.])([@]{1})(?![.-])[a-zA-Z0-9-.]+(?<![-.])$";
            String email = InputStringOfRegexWithMessage(mess, "Please enter email with format <account name>@<domain>", regex);
            return email;
        }
        public static String InputPhoneNumber(int minLength, String mess)
        {
            String regex = @"^[0-9]{" + minLength + @",}$";
            String phoneNum = InputStringOfRegexWithMessage(mess, "Invalid phone number!!", regex).Replace("\\s+", "");
            return phoneNum;
        }

        public static String NormalizeName(string name)
        {
            String res = "";
            String regex = "\\s+";
            string[] strings = name.Trim().Split(regex);
            string[] nameArr = strings;
            for (int i = 0; i < nameArr.Length; i++)
            {
                nameArr[i] = nameArr[i].ToLower();
                res += nameArr[i].Substring(0, 1) + nameArr[i].Substring(1);
                if (i < nameArr.Length) res += " ";
            }
            return res;
        }
        public static Boolean PressYNToContinue(String mess)
        {
            Boolean res = "Y".Equals(InputStringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
            return res;
        }

        public static int GetUserChoiceAsIntNumber(int lowerbound, int upperbound, String mess)
        {
            int res;
            while (true)
            {
                Console.Write(mess);
                res = Convert.ToInt32(Console.ReadLine());
                if (res >= lowerbound && res <= upperbound) return res;
                else Console.WriteLine("Invalid choice! You'll have to input again!");
            }
        }
        public static String getNonEmptyString(String mess)
        {
            String ret = "";
            while (true)
            {
                Console.Write(mess);
                ret = Regex.Replace(Console.ReadLine(), "\\s+", " ");
                if (ret.Equals(""))
                {
                    Console.WriteLine("Please input Non-Empty String!!!");
                    continue;
                }
                return ret;
            }
        }
        public static void DisplayEmployeesInfor(List<Employee> list, String mess, String emptyListNoti)
        {
            if (list == null || list.Count == 0)
            {
                Console.WriteLine(emptyListNoti);
                return;
            }
            Console.WriteLine(mess);
            int counter = 1;
            foreach (Employee employee in list)
            {
                Console.WriteLine(employee + "\n");
                //Console.WriteLine("Employee number " + counter++ + ":\n" + employee + "\n");
            }
        }

    }
}
