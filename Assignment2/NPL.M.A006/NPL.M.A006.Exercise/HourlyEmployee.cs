﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class HourlyEmployee : Employee
    {
        private double Wage { get; set; }
        private double WorkingHour { get; set; }

        public HourlyEmployee(String SSN, String FirstName, String LastName, DateTime BirthDate, String Phone, String Email, double Wage, double WorkingHour)
        {
            this.SSN = SSN;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.BirthDate = BirthDate;
            this.Phone = Phone;
            this.Email = Email;
            this.Wage = Wage;
            this.WorkingHour = WorkingHour;
        }

        public HourlyEmployee(double wage, double workingHour)
        {
            Wage = wage;
            WorkingHour = workingHour;
        }

        public HourlyEmployee()
        {
        }

        public override string ToString()
        {
            return base.ToString() + String.Format("{0, 12} {1, 12}", Wage, WorkingHour);
        }
        public void Display()
        {

        }
    }
}
