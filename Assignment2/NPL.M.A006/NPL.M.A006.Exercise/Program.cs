﻿using NPL.M.A006.Exercise;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("========= Assignment 01 - EmployeeManagement =========\n");
        String menu = "Please select the area you require:";
        menu.Split(" ", 3);
        menu += "\n1. Import Employee";
        menu += "\n\t1.1 Salaried Employees";
        menu += "\n\t1.2 Hourly Employees";
        menu += "\n2. Display Employees Information";
        //menu += "\n\t2.1 Salaried Employees";
        //menu += "\n\t2.2 Hourly Employees";
        //menu += "\n\t2.3 All Employees";
        menu += "\n3. Search Employee";
        menu += "\n\t3.1 Search by employee type";
        menu += "\n\t3.2 Search by employee name";
        menu += "\n4. Exit";

        EmployeeList elist = init();
        do
        {
            Console.WriteLine(menu);
            //EmployeeList list = new EmployeeList();
            int choice = UDF.GetUserChoiceAsIntNumber(1, 4, "Enter menu option number: ");
            switch (choice)
            {
                case 1:
                    elist.AddWithChoice();
                    Console.WriteLine();
                    break;
                case 2:
                    Console.WriteLine();
                    elist.DisplayEmployeesInformation();
                    break;
                case 3:
                    elist.SearchEmployee();
                    break;
                case 4:
                    Console.WriteLine("Exit programm!");
                    Console.ReadLine();
                    break;
            }
            if (choice == 4) break;
        } while (UDF.PressYNToContinue("Press Y to continue, Press N to stop the programm!\nYour choice: "));
        Console.Read();
    }

    public static EmployeeList init()
    {
        EmployeeList elist = new EmployeeList();
        elist.Add(new SalariedEmployee("SE01", "Khai Van", "Pham", DateTime.Parse("07/24/2002"), "0384478185", "cbnlolivan@gmail.com", 24, 7, 2407));
        elist.Add(new SalariedEmployee("SE02", "Khanh Linh", "Nguyen", DateTime.Parse("11/29/2004"), "", "", 29, 22, 2911));
        elist.Add(new SalariedEmployee("SE03", "Thuy Dung", "Le", DateTime.Parse("07/29/2003"), "", "", 29, 7, 2907));
        elist.Add(new SalariedEmployee("SE04", "Hong Ngoc", "Tran", DateTime.Parse("10/19/2001"), "", "", 19, 20, 1910));
        elist.Add(new SalariedEmployee("SE05", "Thu Trang", "Nguyen Thi", DateTime.Parse("04/12/2002"), "", "", 12, 4, 1204));


        elist.Add(new HourlyEmployee("HE01", "Naviank", "Pham", DateTime.Parse("07/24/2002"), "0384478185", "vaniuthutrang@gmail.com", 24, 7));
        elist.Add(new HourlyEmployee("HE02", "Ha Trang", "Nguyen", DateTime.Parse("05/28/2002"), "", "", 29, 11));
        elist.Add(new HourlyEmployee("HE03", "Thuy Linh", "Do", DateTime.Parse("12/08/2003"), "", "", 29, 7));
        elist.Add(new HourlyEmployee("HE04", "Thanh Lam", "Ngo Thi", DateTime.Now, "", "", 19, 10));
        elist.Add(new HourlyEmployee("HE05", "Ha Chi", "Than", DateTime.Now, "", "", 12, 4));
        return elist;
    }
}