﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal class EmployeeList
    {
        private List<Employee> list { get; set; }
        public EmployeeList()
        {
            list = new List<Employee>();
        }
        public EmployeeList(List<Employee> employeeList)
        {
            this.list = employeeList;
        }
        //  Display list
        private void DisplaySalariedEmp(String mess)
        {
            List<Employee> displayList = new List<Employee>();
            foreach (Employee emp in list)
            {
                if (emp.GetType() == typeof(SalariedEmployee)) displayList.Add(emp);
            }
            UDF.DisplayEmployeesInfor(displayList, mess, "There are no salaried employees!");
        }
        private void DisplayHourlyEmp(String mess)
        {
            List<Employee> displayList = new List<Employee>();
            foreach (Employee emp in list)
            {
                if (emp.GetType() == typeof(HourlyEmployee)) displayList.Add(emp);
            }
            UDF.DisplayEmployeesInfor(displayList, mess, "There are no salaried employees!");
        }
        public void DisplayEmployeesInformation()
        {
            String mess = "Please select the type of employee to display:";
            mess += "\n1. Salaried Employees";
            mess += "\n2. Hourly Employees";
            mess += "\n3. All Employees";
            mess += "\n0. Back to menu";
            mess += "\nYour choice is: ";
            String se = String.Format("{0, -7} {1, -24} {2, -24} {3, -12} {4, -24}", "SSN", "Full Name", "BirthDate", "Phone", "Email");
            //do
            //{
                //int option = UDF.GetUserChoiceAsIntNumber(0, 3, mess);
                //switch (option)
                //{
                //    case 1:
                //        DisplaySalariedEmp(se + String.Format("{0, 12} {1, 12} {2, 12}", "Commission", "GrossSales", "BasicSalary") + "\n");
                //        break;
                //    case 2:
                //        DisplayHourlyEmp(se + String.Format("{0, 12} {1, 12}", "Wage", "WorkHour") + "\n");
                //        break;
                //    case 3:
                        Console.WriteLine("\nSalaried employee: ");
                        DisplaySalariedEmp(se + String.Format("{0, 12} {1, 12} {2, 12}", "Commission", "GrossSales", "BasicSalary") + "\n");
                        Console.WriteLine("Hourly employee: ");
                        DisplayHourlyEmp(se + String.Format("{0, 12} {1, 12}", "Wage", "WorkHour") + "\n");
                //        break;
                //    default:
                //        break;
                //}
                //if (option == 0) break;
            //} while (UDF.PressYNToContinue("Press Y to choose other display option, N to stop!\nYour choice: "));
        }
        //  Add new employees
        public void Add(Employee employee)
        {
            if (GetEmployeeOfSSN(employee.SSN) == null)
                list.Add(employee);
            else
            {
                Console.WriteLine("Failed to add new employee!");
                Console.WriteLine("The SSN " + employee.SSN + "has been existed!");
            }
        }
        public void AddWithChoice()
        {
            String mess = "\nPlease select the type of employee u want to you add:";
            mess += "\n1. Salaried Employees";
            mess += "\n2. Hourly Employees";
            mess += "\n0. Back to menu";
            mess += "\nYour choice is: ";
            do
            {
                int option = UDF.GetUserChoiceAsIntNumber(0, 2, mess);
                switch (option)
                {
                    case 1:
                        SalariedEmployee se = InputSalariedEmployee();
                        Add(se);
                        break;
                    case 2:
                        HourlyEmployee he = InputHourlyEmployee();
                        Add(he);
                        break;
                    default:
                        break;
                }
                if (option == 0) break;
            } while (UDF.PressYNToContinue("Press Y to continue adding employee, N to stop!\nYour choice: "));
        }
        /// <summary>
        /// This function allows users to input information of Hourly Employee
        /// </summary>
        /// <returns>
        /// Return the inputted employee
        /// </returns>
        private HourlyEmployee InputHourlyEmployee()
        {
            Console.WriteLine("Input common information of a Normal Employee: ");
            String ssn = UDF.getNonEmptyString("Enter SSN: ");
            String fNameRaw = UDF.InputStringOfRegexWithMessage("Enter first name: ", "Wrong name format!!!", @"^([A-Za-z ]+)$");
            String fName = UDF.NormalizeName(fNameRaw);
            String lNameRaw = UDF.InputStringOfRegexWithMessage("Enter last name: ", "Wrong name format!!!", @"^[A-Za-z ]+$");
            String lName = UDF.NormalizeName(lNameRaw);
            DateTime birthDate = UDF.InputBirthDate("Input birthdate: ");
            String phone = UDF.InputPhoneNumber(7, "Input phone number (at least 7 digits): ");
            String email = UDF.InputEmail("Input email: ");

            Console.WriteLine("Input additional information of Hourly Employee: ");
            String wageRaw = UDF.InputStringOfRegexWithMessage("Wage of employee: ", "Wrong number format!!!", @"^[0-9]+\.?([0-9]+)?$");
            double wage = double.Parse(wageRaw);
            String hourRaw = UDF.InputStringOfRegexWithMessage("Working hour of employee: ", "Wrong number format!!!", @"^[0-9]+\.?([0-9]+)?$");
            double workingHour = double.Parse(hourRaw);

            HourlyEmployee employee = new HourlyEmployee(ssn, fName, lName, birthDate, phone, email, wage, workingHour);
            return employee;
        }

        /// <summary>
        /// This function allows users to input information of Salaried Employee
        /// </summary>
        /// <returns>
        /// Return the inputted employee
        /// </returns>
        private SalariedEmployee InputSalariedEmployee()
        {
            Console.WriteLine("Input common information of a Normal Employee: ");
            String ssn = UDF.getNonEmptyString("Enter SSN: ");
            String fNameRaw = UDF.InputStringOfRegexWithMessage("Enter first name: ", "Wrong name format!!!", @"^([A-Za-z ]+)$");
            String fName = UDF.NormalizeName(fNameRaw);
            String lNameRaw = UDF.InputStringOfRegexWithMessage("Enter last name: ", "Wrong name format!!!", @"^([A-Za-z ]+)$");
            String lName = UDF.NormalizeName(lNameRaw);
            DateTime birthDate = UDF.InputBirthDate("Input birthdate: ");
            String phone = UDF.InputPhoneNumber(7, "Input phone number (at least 7 digits): ");
            String email = UDF.InputEmail("Input email: ");

            Console.WriteLine("Input additional information of Salaried Employee: ");
            String crRaw = UDF.InputStringOfRegexWithMessage("Commision rate of employee: ", "Wrong number format!!!", @"^[0-9]+\.?([0-9]+)?$");
            double commissionRate = double.Parse(crRaw);
            String gsRaw = UDF.InputStringOfRegexWithMessage("Gross sales of employee: ", "Wrong number format!!!", @"^[0-9]+\.?([0-9]+)?$");
            double grossSales = double.Parse(gsRaw);
            String bsRaw = UDF.InputStringOfRegexWithMessage("Basic salary of employee: ", "Wrong number format!!!", @"^[0-9]+\.?([0-9]+)?$");
            double basicSalary = double.Parse(bsRaw);

            SalariedEmployee employee = new SalariedEmployee(ssn, fName, lName, birthDate, phone, email, commissionRate, grossSales, basicSalary);
            return employee;
        }
        //  Get employee by index
        public Employee GetEmployeeOfSSN(String SSN)
        {
            foreach (Employee emp in list)
            {
                if (emp.SSN == SSN) return emp;
            }
            return null;
        }
        //  Search Employee
        public void SearchEmployeeByType()
        {
            String mess = "Please select the type of employee to display:";
            mess += "\n1. Salaried Employees";
            mess += "\n2. Hourly Employees";
            mess += "\n0. Back to menu";
            mess += "\nYour choice is: ";
            do
            {
                int option = UDF.GetUserChoiceAsIntNumber(0, 2, mess);
                String se = String.Format("{0, -7} {1, -24} {2, -24} {3, -12} {4, -24}", "SSN", "Full Name", "BirthDate", "Phone", "Email");
                switch (option)
                {
                    case 1:
                        DisplaySalariedEmp(se + String.Format("{0, 12} {1, 12} {2, 12}", "Commission", "GrossSales", "BasicSalary") + "\n");
                        break;
                    case 2:
                        DisplayHourlyEmp(se + String.Format("{0, 12} {1, 12}", "Wage", "WorkHour") + "\n");
                        break;
                    default:
                        break;
                }
                if (option == 0) break;
            } while (UDF.PressYNToContinue("Press Y to choose other option, N to stop!\nYour choice: "));
        }
        public void SearchEmployee()
        {
            String mess = "\nPlease select the method of search to execute:";
            mess += "\n1. Search by Employee type";
            mess += "\n2. Search by Employee name";
            mess += "\n0. Back to menu";
            mess += "\nYour choice is: ";
            String regex = @"^([A-Za-z ]+)$";
            do
            {
                int option = UDF.GetUserChoiceAsIntNumber(0, 2, mess);
                switch (option)
                {
                    case 1:
                        SearchEmployeeByType();
                        break;
                    case 2:
                        String searchvalue = UDF.InputStringOfRegexWithMessage("Input name as a string to search: ", "Wrong name format!!!", regex);
                        List<Employee> foundEmployee = SearchEmployeeByName(searchvalue);
                        UDF.DisplayEmployeesInfor(foundEmployee, "Found employee: ", "Not found any employee with keyword " + searchvalue);
                        break;
                    default:
                        break;
                }
                if (option == 0) break;
            } while (UDF.PressYNToContinue("Press Y to continue searching, N to stop!\nYour choice: "));
        }
        public List<Employee> SearchEmployeeByName(String searchvalue)
        {
            List<Employee> result = new List<Employee>();
            foreach (Employee emp in this.list)
            {
                if ((emp.FirstName + " " + emp.LastName).Contains(searchvalue))
                    result.Add(emp);
            }
            return result;
        }
        //  Generated code        
        public void Remove(Employee employee)
        {
            list.Remove(employee);
        }
        public void Clear()
        {
            list.Clear();
        }
        public void Insert(int index, Employee employee)
        {
            list.Insert(index, employee);
        }
        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }
        public bool Contains(Employee employee)
        {
            return list.Contains(employee);
        }
        public int IndexOf(Employee employee)
        {
            return list.IndexOf(employee);
        }
    }
}
