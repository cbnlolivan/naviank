﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006.Exercise
{
    internal abstract class Employee : IComparable<Employee>
    {
        public string SSN {  get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Phone {  get; set; }
        public string Email { get; set; }

        public Employee() { }
        public Employee(string SSN, string FirstName,string LastName)
        {
            this.SSN = SSN;
            this.FirstName = FirstName;
            this.LastName = LastName;
        }
        public Employee(String SSN, String FirstName,String LastName, DateTime BirthDate, String Phone, String Email)
        {
            this.SSN = SSN;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.BirthDate = BirthDate;
            this.Phone = Phone;
            this.Email = Email;
        }
        public void Display()
        {
        }
        public override string ToString()
        {
            String res = "";
            res += String.Format("{0, -7} {1, -24} {2, -24} {3, -12} {4, -24}", SSN, FirstName + " " + LastName, BirthDate, Phone, Email);
            return res;
        }
        public int CompareTo(Employee? other)
        {
            return String.Compare(FirstName + " " + LastName, other.FirstName + " " + other.LastName);
        }
    }
}
