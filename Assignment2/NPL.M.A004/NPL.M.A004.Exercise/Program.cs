﻿/*
   Created by Naviank
*/
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("==== This programm do something ====");
        String regex = @"^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(19|20)\d\d$";
        do
        {
            String TimeRaw = InputStringOfRegexWithMessage("Enter date (follow the format dd/mm/yyyy): ", "Wrong date format!!!", regex);
            String[] TimeArr = TimeRaw.Split("/");
            int Year = 2023;
            int Month = 1;
            int Date = 1;
            Date = Int32.Parse(TimeArr[0]);
            Month = Int32.Parse(TimeArr[1]);
            Year = Int32.Parse(TimeArr[2]);
            DateTime Time = new DateTime(Year, Month, Date);
            Console.WriteLine();
            Console.WriteLine("The inputted day is: " + Time.ToString("D"));
            Console.WriteLine("The last day of this month is: " + GetLastDayOfMonth(Time).ToString("D"));
            Console.WriteLine("The first day of this month is: " + GetFirstDayOfMonth(Time).ToString("D"));
            Console.WriteLine("The number of working day in this month is: " + CountWorkingDays(Time));
            Console.WriteLine();
        } while (PressYNToContinue("Press Y to continue, N to stop the programm!: "));
        //DateTime d1 = new DateTime(2015, 11, 10);
        //DateTime d2 = new DateTime(2018, 11, 10);
        //Console.WriteLine(d1.CompareTo(d2));
    }

    private static int CountWorkingDays(DateTime Time)
    {
        int res = 0;
        DateTime tmp = GetFirstDayOfMonth(Time);
        while (tmp.Month == Time.Month)
        {
            if ((int)tmp.DayOfWeek != 0 && (int)tmp.DayOfWeek != 6)
                res++;
            tmp = tmp.AddDays(1);
        }
        return res;
    }

    private static DateTime GetLastDayOfMonth(DateTime Time)
    {
        DateTime tmp = Time;
        while (tmp.AddDays(1).Month == Time.Month)
        {
            tmp = tmp.AddDays(1);
        }
        return tmp;
    }
    private static DateTime GetFirstDayOfMonth(DateTime Time)
    {
        DateTime tmp = Time;
        while (tmp.AddDays(-1).Month == Time.Month)
        {
            tmp = tmp.AddDays(-1);
        }
        return tmp;
    }
    public static String InputStringOfRegexWithMessage(String message, String error, String regex)
    {
        while (true)
        {
            Console.Write(message);
            String res = Console.ReadLine();
            if (Regex.IsMatch(res, regex))
            {
                Console.WriteLine("Accepted input:" + res);
                Console.WriteLine();
                return res;
            }
            else
            {
                Console.Write(error);
            }
        }
    }
    public static Boolean PressYNToContinue(String mess)
    {
        Boolean res = "Y".Equals(InputStringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
        return res;
    }
}