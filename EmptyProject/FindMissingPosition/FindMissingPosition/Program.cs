﻿using System.Reflection.Metadata.Ecma335;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");

        Console.Write("Size: ");
        int n = int.Parse(Console.ReadLine());
        List<int> list = new List<int>();
        for (int i = 0; i < n; i++)
        {
            Console.Write("Value: ");
            list.Add(int.Parse(Console.ReadLine()));
        }
        list.Sort();
        int[] arr = list.ToArray();

        list.Clear();
        int curIndex = 0;
        int maxIndex = (arr[n - 1] - 1) / 2;
        for (int i = 0; i < maxIndex; i++)
        {
            int tmp = 2 * i + 1;
            if (arr[curIndex] == tmp)
            {
                curIndex++;
            }
            else if (arr[curIndex] < tmp)
            {
                Console.WriteLine("Exception!");
                return;
            }
            else if (arr[curIndex] > tmp)
            {
                list.Add(tmp);
            }
        }
        foreach (int i in list)
        {
            Console.Write(i + ", ");
        }
    }
}