﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingProject
{
    internal class Student
    {
        private string PrivateName { get; set; }
        public int Id { get; set; }
        public string Name
        {
            get { return PrivateName; }
            set
            {
                if (PrivateName != value)
                {
                    Console.WriteLine("Call setter");
                    PrivateName = value;
                }
            }
        }

        public Student()
        {
        }

        public Student(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
