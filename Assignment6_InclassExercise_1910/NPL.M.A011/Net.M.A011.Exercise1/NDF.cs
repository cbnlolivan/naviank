﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise1
{
    internal static class NDF
    {
        public static int CountInt(this ArrayList array)
        {
            return array.OfType<int>().Count();
            //other way:
            //return array.CountOf<int>();
            //return array.CountOf(typeof(int));
            //int count = 0;
            //foreach (var item in array)
            //{
            //    if (typeof(int) == item.GetType())
            //        count++;
            //}
            //return count;
        }
        public static int CountOf(this ArrayList array, Type dataType)
        {
            int count = 0;
            foreach (var item in array)
            {
                if ((dataType) == (item.GetType()))
                    count++;
            }
            return count;
            //other way:
            //return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
        }

        public static int CountOf<T>(this ArrayList array)
        {
            return array.OfType<T>().Count();
            //other way:
            //int count = 0;
            //foreach (var item in array)
            //{
            //    if (item.GetType() == typeof(T))
            //        count++;
            //}
            //return count;
        }
        public static T MaxOf<T>(this ArrayList array) where T : IComparable
        {
            Type type = typeof(T);
            if (type.IsPrimitive && type != typeof(bool) && type != typeof(char))
            //other way:
            //if (typeof(T) == typeof(double) || typeof(T) == typeof(int) || typeof(T) == typeof(float) || typeof(T) == typeof(decimal))
            {
                T max = default(T);
                foreach (var item in array)
                {
                    try
                    {
                        if (typeof(T) == item.GetType())
                        {
                            T tmpItem = ((T)Convert.ChangeType(item, typeof(T)));
                            if (tmpItem.CompareTo(max) > 0)
                            {
                                max = tmpItem;
                            }
                            //other way:
                            //T tmpItem = ((T)Convert.ChangeType(item, typeof(T)));
                            //if (Comparer<T>.Default.Compare(tmpItem, max) > 0)
                            //{
                            //    max = tmpItem;
                            //}
                        }
                    }
                    catch (Exception ex) { }
                }
                return max;
            }
            else
            {
                throw new Exception("Không tìm thấy phần tử có kiểu số cụ thể trong ArrayList.");
            }
            //The function above was too long, here is a shorter way to perform this function:
            /*
            var numericItems = array.OfType<T>();
            if (numericItems.Any())
            {
                return numericItems.Max();
            }
            throw new InvalidOperationException("Không tìm thấy phần tử có kiểu số cụ thể trong ArrayList.");             
             */
        }
    }

}
