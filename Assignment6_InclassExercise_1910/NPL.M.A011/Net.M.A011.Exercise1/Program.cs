﻿using Net.M.A011.Exercise1;
using System.Collections;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        ArrayList list = new ArrayList();
        list.Add("Hieu");
        list.Add("Hoang");
        list.Add(1);
        list.Add(2);
        list.Add("Trong");
        list.Add(3.9d);
        Console.WriteLine("CountInt(): " + list.CountInt());
        Console.WriteLine("CountOf(typeof(int): " + list.CountOf(typeof(int)));
        Console.WriteLine("CountOf<string>(): " + list.CountOf<string>());
        Console.WriteLine("MaxOf<int>(): " + list.MaxOf<int>());
        Console.WriteLine(3.9d > 2);
    }

}