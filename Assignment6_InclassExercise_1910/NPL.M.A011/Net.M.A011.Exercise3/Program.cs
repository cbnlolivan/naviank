﻿using Net.M.A011.Exercise3;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        int[] intArr = { 1, 2, 3, 5, 7, 3, 2 };
        string[] stringArr = { "Hoang", "Trong", "A", "Hoang", "Trong", "Hieu" };

        Console.WriteLine("Last index of 3 in int array: " + intArr.LastIndexOf<int>(3));
        Console.WriteLine("last index of \"Hieu\" in string array: " + stringArr.LastIndexOf<string>("Trong"));
    }
}