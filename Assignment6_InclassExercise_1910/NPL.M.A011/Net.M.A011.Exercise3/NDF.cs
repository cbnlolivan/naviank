﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise3
{
    internal static class NDF
    {
        public static int LastIndexOf<T>(this T[] array, T elementValue)
        {
            int res = -1;
            for (int i = 0; i < array.Length; i++)
            {
                if (Comparer<T>.Default.Compare(array[i], elementValue) == 0)
                //The other way: 
                // if (array[i].Equals(elementValue))
                // if (Object.Equals(array[i], elementValue))
                {
                    res = i;
                }
            }
            return res;
        }
    }
}
