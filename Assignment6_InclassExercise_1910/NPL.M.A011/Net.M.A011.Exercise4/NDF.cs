﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise4
{
    internal static class NDF
    {
        public static int[] RemoveDuplicate(this int[] array)
        {
            List<int> result = new List<int>();
            foreach (int i in array)
            {
                if (!result.Contains(i))
                {
                    result.Add(i);
                }
            }
            return result.ToArray();
        }
        public static int ElementOfOrder2(this int[] array)
        {

            if (array.Length < 2)
            {
                throw new InvalidOperationException("Mảng phải có ít nhất 2 phần tử.");
            }
            // Selection sort (dan vcl)
            int res = 0;
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] > array[j])
                    {
                        (array[i], array[j]) = (array[j], array[i]);
                    }
                }
            }
            return array[array.Length - 2];
            // An other way:
            // return array.OrderByDescending(x => x).ElementAt(1);
        }

        public static T ElementOfOrder<T>(this T[] array, int orderLargest)
        {
            if ((orderLargest < 1) || (orderLargest > array.Length))
            {
                throw new Exception("Index out of array size!!!");
            }
            // Selection sort
            for (int i = 0; i <= array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (Comparer<T>.Default.Compare(array[i], array[j]) > 0)
                    {
                        (array[i], array[j]) = (array[j], array[i]);
                    }
                }
            }
            return array[array.Length - orderLargest];

            // Cach khac xin vcl hon:
            // return array.OrderByDescending(x => x).ElementAt(orderLargest - 1);
        }
    }
}
