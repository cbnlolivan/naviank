﻿using Net.M.A011.Exercise4;

internal class Program
{
    private static void Main(string[] args)
    {
        int[] intArr = new int[] { 3, 2, 5, 6, 1, 7, 7, 5, 2 };
        
        //Console.WriteLine("Demo ElementOfOrder2 Function: ");
        Console.WriteLine("Element of order 3 is " + intArr.RemoveDuplicate().ElementOfOrder2());
        //Console.WriteLine("Demo ElementOfOrder<T> Function: ");
        Console.WriteLine("Element of order 2 is " + intArr.RemoveDuplicate().ElementOfOrder(2));
        Console.WriteLine("Element of order 3 is " + intArr.RemoveDuplicate().ElementOfOrder(3));
        Console.WriteLine("Element of order 20 is " + intArr.RemoveDuplicate().ElementOfOrder(20));

    }
}
