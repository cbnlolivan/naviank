﻿using Net.M.A011.Exercise2;
using System.Collections;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        int[] intArr = new int[] { 1, 2, 3, 3, 5, 6, 5, 2 };
        string[] stringArr = new string[] { "Hung", "Vu", "Van", "Hung", "Quang", "Huy", "Vu" };
        foreach (int i in intArr.RemoveDuplicate())
        {
            Console.Write(i + ", ");
        }
        Console.WriteLine();
        for (int i = 0; i < stringArr.RemoveDuplicate<string>().Length; i++)
        {
            if (i == 0) Console.Write("{ \"" + stringArr[i] + "\"");
            else if (i == stringArr.RemoveDuplicate<string>().Length - 1) Console.Write(", \"" + stringArr[i] + "\" }");
            else Console.Write(", \"" + stringArr[i] + "\"");
        }
    }

}