﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise2
{
    internal static class NDF
    {
        public static int[] RemoveDuplicate(this int[] array)
        {
            List<int> result = new List<int>();
            foreach (int i in array)
            {
                if (!result.Contains(i))
                {
                    result.Add(i);
                }
            }
            return result.ToArray();
        }
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            List<T> result = new List<T>();
            foreach (T t in array)
            {
                if (!result.Contains(t))
                {
                    result.Add(t);
                }
            }
            return result.ToArray();
        }
       
    }
}
