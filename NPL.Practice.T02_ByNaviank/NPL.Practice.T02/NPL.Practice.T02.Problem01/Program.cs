﻿using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        string content = getNonEmptyString("Content: ");
        int maxLength = InputSomethingOfType<int>("Max Length: ", "Only integer accepted!", @"^\d+$");
        Console.WriteLine(content.Length);
        Console.WriteLine(GetArticleSummary(content, maxLength));
    }
    static public string GetArticleSummary(string content, int maxLength)
    {
        // Neu do dai rut gon lon hon do dai doan van, tra ve toan bo doan van.
        int length = content.Length;
        if (maxLength >= length)
            return content;
        else
        {
            int lastIndexOfSubString = maxLength;
            // Duyet qua cac vi tri cua string theo chieu nguoc tu 'maxLength' ve 0
            // Neu gap dau cach (' ') o vi tri nao thi gan gia tri vi tri do cho bien 'lastIndexOfSubString'
            for (int i = maxLength; i >= 0; i--)
            {
                if (content[i].Equals(' '))
                {
                    lastIndexOfSubString = i;
                    break;
                }
            }
            // Tra ve substring tu 0 den vi tri 'lastIndexOfSubString'
            return content.Substring(0, lastIndexOfSubString) + "...";
            // Cach 2:
            //return content.Substring(0, (content.Substring(0, maxLength).LastIndexOf(" ")));
        }
    }
    public static T InputSomethingOfType<T>(string message, string errorMessage, string regex)
    {
        T result = default(T);
        while (true)
        {
            Console.Write(message);
            string tmp = Console.ReadLine();
            if (Regex.IsMatch(tmp, regex))
            {
                try
                {
                    Console.WriteLine();
                    result = (T)Convert.ChangeType(tmp, typeof(T));
                    return result;
                }
                catch (Exception ex) { }
            }
            else
            {
                Console.WriteLine(errorMessage);
            }
        }
    }
    public static string getNonEmptyString(string mess)
    {
        string ret = "";
        while (true)
        {
            Console.Write(mess);
            ret = Regex.Replace(Console.ReadLine(), "\\s+", " ");
            if (ret.Equals(""))
            {
                Console.WriteLine("Please input Non-Empty string!!!");
                continue;
            }
            return ret;
        }
    }
    public static string ReadLineOrCtrlEnter(string mess)
    {
        string retstring = "";
        string curLine = "";
        int curIndex = 0;
        Console.WriteLine(mess);
        do
        {
            ConsoleKeyInfo readKeyResult = Console.ReadKey(true);
            // handle Ctrl + Enter
            if (readKeyResult.Key == ConsoleKey.Enter && (readKeyResult.Modifiers & ConsoleModifiers.Control) != 0)
            {
                if (!(curIndex == 0))
                {
                    retstring += curLine;
                    Console.WriteLine();
                }
                return retstring;
            }
            // handle Enter
            if (readKeyResult.Key == ConsoleKey.Enter)
            {
                Console.WriteLine();
                if (curIndex != 0)
                {
                    retstring += curLine;
                    retstring += "\n";
                    curLine = "";
                }
                curIndex = 0;
            }
            // handle backspace
            else if (readKeyResult.Key == ConsoleKey.Backspace)
            {
                if (curIndex > 0)
                {
                    curLine = curLine.Remove(curLine.Length - 1);
                    Console.Write(readKeyResult.KeyChar);
                    Console.Write(' ');
                    Console.Write(readKeyResult.KeyChar);
                    curIndex--;
                }
            }
            else
            // handle all other keypresses
            {
                curLine += readKeyResult.KeyChar;
                Console.Write(readKeyResult.KeyChar);
                curIndex++;
            }
        }
        while (true);
    }
}