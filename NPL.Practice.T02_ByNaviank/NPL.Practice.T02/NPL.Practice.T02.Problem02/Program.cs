﻿using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {

        int length = InputSomethingOfType<int>("Array length: ", "Wrong format!!!", @"^[+-]?\d+$");
        int[] intputArray = new int[length];
        for (int i = 0; i < length; i++)
        {
            intputArray[i] = InputSomethingOfType<int>("Value at index " + i +": ", "Wrong format!!!", @"^[+-]?\d+$");
        }
        int subLength = InputSomethingOfType<int>("Sublength to performing function: ", "Wrong format!!!", @"^[+-]?\d+$");
        
        PrintIntArray(intputArray, "\nInputed array:");
        Console.Write("The maximum contiguous subarray sum of it: ");
        Console.WriteLine(FindMaxSubArray(intputArray, subLength));
    }

    static public int FindMaxSubArray(int[] inputArray, int subLength)
    {
        if (inputArray.Length < subLength)
        {
            throw new ArgumentException("The sublength is greater than array length!!!");

        }
        int maxValue = 0;
        for (int i = 0; i < subLength; i++)
        {
            maxValue += inputArray[i];
        }
        inputArray.Max();
        for (int i = subLength; i < inputArray.Length; i++)
        {
            int tmp = maxValue - inputArray[i - subLength] + inputArray[i];
            if (tmp > maxValue)
                maxValue = tmp;
        }
        return maxValue;
    }
    public static T InputSomethingOfType<T>(string message, string errorMessage, string regex)
    {
        T result = default(T);
        while (true)
        {
            Console.Write(message);
            string tmp = Console.ReadLine();
            if (Regex.IsMatch(tmp, regex))
            {
                try
                {
                    Console.WriteLine();
                    result = (T)Convert.ChangeType(tmp, typeof(T));
                    return result;
                }
                catch (Exception ex) { }
            }
            else
            {
                Console.WriteLine(errorMessage);
            }
        }
    }
    public static void PrintIntArray(int[] inputArray, string mess)
    {
        Console.WriteLine(mess);
        for (int i = 0;i < inputArray.Length;i++)
        {
            if (i == 0)
            {
                Console.Write("[ " + inputArray[i]);
            } else if (i < inputArray.Length - 1)
            {
                Console.Write("; " + inputArray[i]);
            } else
            {
                Console.Write(inputArray[i] + " ]");
            }
        }
    }
}