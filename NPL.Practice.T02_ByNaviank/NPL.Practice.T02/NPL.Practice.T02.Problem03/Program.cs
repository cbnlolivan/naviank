﻿using NPL.Practice.T02.Problem03;
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        /*
        string input = "07/24/2023";
        string pattern = @"^(1[0-2]|0?[1-9])\/(3[01]|[12][0-9]|0?[1-9])\/(20([0-1][0-9]|2[0-3]))$";
        Match match = Regex.Match(input, pattern);
        if (match.Success)
        {
            Console.WriteLine(match.Groups[0].Value);  // Output: "192.168.0."
            Console.WriteLine(match.Groups[1].Value);  // Output: "0."
            Console.WriteLine(match.Groups[2].Value);  // Output: "0."
            Console.WriteLine(match.Groups[3].Value);  // Output: "0."
        }
        */
        Console.WriteLine("==== Student grade management ====");
        List<Student> list = new()
        {
            new Student(1, "Pham Khai Van", DateTime.Parse("07/24/2002"), 10, 10, 10),
            new Student(2, "Tran Hong Ngoc", DateTime.Parse("10/19/2001"), 10, 9, 10),
            new Student(3, "Le Thuy Dung", DateTime.Parse("07/29/2003"), 7, 10, 9),
            new Student(4, "Do Ha Trang", DateTime.Parse("05/28/2002"), 9, 10, 8),
            new Student(5, "Nguyen Thi Thu Trang", DateTime.Parse("04/12/2002"), 7, 2, 5),
            new Student(6, "Do Thuy Linh", DateTime.Parse("12/08/2003"), 9, 5, 8),
            new Student(7, "Tran Ngoc Huyen", DateTime.Parse("11/12/2003"), 9, 7, 9.5m),
            new Student(8, "Tran Ha Ngoc Thao", DateTime.Parse("10/11/2002"), 4, 6, 8),
            new Student(9, "Nguyen Thi Thuy Tien", DateTime.Parse("03/09/2002"), 9.5m, 5m, 9.5m)
        };

        Manager manager = new(list);
        int choice;

        do
        {
            Console.WriteLine("\tMenu options:");
            Console.WriteLine("\n1. Display students list");
            Console.WriteLine("\n2. Add students to list");
            Console.WriteLine("\n3. Edit student information");
            Console.WriteLine("\n4. ");
            Console.WriteLine("\n5. ");
            Console.WriteLine("\n0. Exit program");
            choice = NDF.GetUserChoiceAsIntNumber(0, 10, "Select an option by entering an integer: ");
            switch (choice)
            {
                case 0:
                    Console.WriteLine("Exit program!!!");
                    break;
                case 1:
                    string[] s = {""};
                    NDF.PrintList<Student>(list, "List of students:", 
                        string.Format("{0,-8}", "ID"), 
                        string.Format("{0,-32}", "  Name"),
                        string.Format("{0,-32}", "  StartDate"),
                        string.Format("{0,-8}","GPA"),
                        string.Format("{0,-20}", "Graduate Level"));
                    break;
                case 2:
                    manager.AddStudentsToList();
                    break;
                case 3:
                    Console.WriteLine("Option 3");
                    manager.EditStudentInformation();
                    break;
                case 4:
                    Console.WriteLine("Option 4");
                    break;
                case 5:
                    Console.WriteLine("Option 5");
                    break;

            }
        } while (choice != 0);
    }
}