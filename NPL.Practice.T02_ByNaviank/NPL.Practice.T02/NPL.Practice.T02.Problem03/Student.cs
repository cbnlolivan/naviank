﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Student : IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel Level { get; set; }

        public Student()
        {
        }

        public Student(int id, string name, DateTime startDate, decimal sqlMark, decimal csharpMark, decimal dsaMark)
        {
            this.Id = id;
            this.Name = name;
            this.StartDate = startDate;
            this.SqlMark = sqlMark;
            this.CsharpMark = csharpMark;
            this.DsaMark = dsaMark;
            this.Graduate();
        }

        public override string? ToString()
        {
            return string.Format("{0,-8}{1,-32}{2,-32}{3,-8}  {4,-20}",
                                 this.Id,
                                 this.Name,
                                 this.StartDate.ToString("MM/dd/yyyy hh:mm:ss tt"),
                                 Math.Round(this.GPA, 2),
                                 this.Level);
        }

        public void Graduate()
        {
            decimal Gpa = (this.CsharpMark + this.DsaMark + this.SqlMark) / 3;
            GraduateLevel level;
            if (Gpa >= 9)
                level = (GraduateLevel)0;
            else if (Gpa >= 8 && Gpa < 9)
                level = (GraduateLevel)1;
            else if (Gpa >= 7 && Gpa < 8)
                level = (GraduateLevel)2;
            else if (Gpa >= 5 && Gpa < 7)
                level = (GraduateLevel)3;
            else
                level = (GraduateLevel)4;

            this.GPA = Gpa;
            this.Level = level;
        }
        public string GetCertificate()
        {
            return "Name: " + this.Name + ", SqlMark: " + this.SqlMark + ", CsharpMark: "
                + this.CsharpMark + ", DasMark: " + DsaMark + ",Gpa: " + this.GPA + ",Level: " + this.Level;
        }

        public void ModifyInformation(bool cName = true, bool cDate = true, bool cSqlMark = true, bool cCsharpMark = true, bool cDsaMark = true)
        {

            if (cName)
                this.Name = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Student name: ", "Wrong name format!", @"^[a-zA-Z ]+$"));
            if (cDate)
            {
                this.StartDate = NDF.InputSomethingOfType<DateTime>("Start date (MM/dd/yyyy): ", "Wrong format!", @"^(1[0-2]|0?[1-9])\/(3[01]|[12][0-9]|0?[1-9])\/(20([0-1][0-9]|2[0-3]))$");
            }
            if (cSqlMark)
                this.SqlMark = NDF.InputSomethingOfType<decimal>("Sql mark: ", "Wrong number format!", @"^(10|(\d\.?\d*))$");
            if (cCsharpMark)
                this.CsharpMark = NDF.InputSomethingOfType<decimal>("C# mark: ", "Wrong number format!", @"^(10|(\d\.?\d*))$");
            if (cDsaMark)
                this.DsaMark = NDF.InputSomethingOfType<decimal>("Dsa mark: ", "Wrong number format!", @"^(10|(\d\.?\d*))$");

            Graduate();
        }

        public void ModifyStudent()
        {
            //string cName = 
        }

    }
}
