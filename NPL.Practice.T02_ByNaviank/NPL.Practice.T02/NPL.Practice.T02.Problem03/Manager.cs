﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Manager
    {
        List<Student> list { get; set; }
        public Manager() { this.list = new List<Student>(); }
        public Manager(List<Student> list) { this.list = list; }

        // Data manipulation
        public Student? GetStudentOfID(int id)
        {
            return this.list.Where(x => x.Id == id).FirstOrDefault();
        }
        public List<Student> GetListStudentsWhoseNamesContain(string input)
        {
            return this.list.Where(x => x.Name.Contains(input)).ToList();
        }


        public Student InputANewStudent()
        {
            Student res = new Student();
            while (true)
            {
                int tmpId = NDF.InputSomethingOfType<int>("Student ID: ", "Wrong format!", @"^\d+$");
                if (GetStudentOfID(tmpId) == null)
                {
                    res.Id = tmpId;
                    break;
                }
                else
                {
                    Console.WriteLine("Student ID " + tmpId + " has existed! Enter an other!");
                }
            }
            res.Name = NDF.NormalizeName(NDF.InputstringOfRegexWithMessage("Student name: ", "Wrong name format!", @"^[a-zA-Z ]+$"));
            res.StartDate = NDF.InputSomethingOfType<DateTime>("Start date (MM/dd/yyyy): ", "Wrong format!", @"^(1[0-2]|0?[1-9])\/(3[01]|[12][0-9]|0?[1-9])\/(20([0-1][0-9]|2[0-3]))$");
            res.SqlMark = NDF.InputSomethingOfType<decimal>("Sql mark: ", "Wrong number format!", @"^(10|(\d\.?\d*))$");
            res.CsharpMark = NDF.InputSomethingOfType<decimal>("Sql mark: ", "Wrong number format!", @"^(10|(\d\.?\d*))$");
            res.DsaMark = NDF.InputSomethingOfType<decimal>("Sql mark: ", "Wrong number format!", @"^(10|(\d\.?\d*))$");

            return res;
        }
        public void AddStudentsToList()
        {
            int count = 1;
            do
            {
                this.list.Add(InputANewStudent());
                Console.WriteLine("Student was added to list!");
                Console.WriteLine("Added " + count + (count == 1 ? " student" : " students"));
            } while (NDF.PressYNToContinue("Continue adding student!? - Y or N: "));
            list.Sort();
        }

        public void EditStudentInformation()
        {
            do
            {
                int id = NDF.InputSomethingOfType<int>("ID of student need to be modified: ", "Wrong format!", @"^\d+$");
                Student s = GetStudentOfID(id);
                if (s != null)
                {
                    s.ModifyInformation();
                }
                else
                {
                    Console.WriteLine("There is no students with id = " + id);
                }
            } while (NDF.PressYNToContinue("Modify other student!? - Y or N: "));
        }

    }
}
